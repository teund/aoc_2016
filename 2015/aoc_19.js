var replacementStr = "Al => ThF|Al => ThRnFAr|B => BCa|B => TiB|B => TiRnFAr|Ca => CaCa|Ca => PB|Ca => PRnFAr|Ca => SiRnFYFAr|Ca => SiRnMgAr|Ca => SiTh|F => CaF|F => PMg|F => SiAl|H => CRnAlAr|H => CRnFYFYFAr|H => CRnFYMgAr|H => CRnMgYFAr|H => HCa|H => NRnFYFAr|H => NRnMgAr|H => NTh|H => OB|H => ORnFAr|Mg => BF|Mg => TiMg|N => CRnFAr|N => HSi|O => CRnFYFAr|O => CRnMgAr|O => HP|O => NRnFAr|O => OTi|P => CaP|P => PTi|P => SiRnFAr|Si => CaSi|Th => ThCa|Ti => BP|Ti => TiTi|e => HF|e => NAl|e => OMg";
var rules = replacementStr.split("|").map(function(s){
	var parts = s.split(" => ");
	return {from:parts[0], to:parts[1]};
});
var medicine = "CRnSiRnCaPTiMgYCaPTiRnFArSiThFArCaSiThSiThPBCaCaSiRnSiRnTiTiMgArPBCaPMgYPTiRnFArFArCaSiRnBPMgArPRnCaPTiRnFArCaSiThCaCaFArPBCaCaPTiTiRnFArCaSiRnSiAlYSiThRnFArArCaSiRnBFArCaCaSiRnSiThCaCaCaFYCaPTiBCaSiThCaSiThPMgArSiRnCaPBFYCaCaFArCaCaCaCaSiThCaSiRnPRnFArPBSiThPRnFArSiRnMgArCaFYFArCaSiRnSiAlArTiTiTiTiTiTiTiRnPMgArPTiTiTiBSiRnSiAlArTiTiRnPMgArCaFYBPBPTiRnSiRnMgArSiThCaFArCaSiThFArPRnFArCaSiRnTiBSiThSiRnSiAlYCaFArPRnFArSiThCaFArCaCaSiThCaCaCaSiRnPRnCaFArFYPMgArCaPBCaPBSiRnFYPBCaFArCaSiAl";

function execAllReplacements(molecule, rules){
	var results = [];
	rules.forEach(function(rule) {
		var from = 0;
		while(true){
			var ix = molecule.indexOf(rule.from, from);
			if(ix === -1)break;
			results.push(molecule.substring(0,ix) + rule.to + molecule.substring(ix + rule.from.length));
			from = ix+1;
		}
		
	});
	return results;

}
function addResultsToSet(molecules, knowledge){
	molecules.forEach(function(m){knowledge[m] = true;});
}
var knowlegde = {};
knowlegde[medicine] = true;
var res = execAllReplacements(medicine, rules);
addResultsToSet(res, knowlegde);

console.log(Object.keys(knowlegde).length);
