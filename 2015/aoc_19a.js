var replacementStr = "Al => ThF|Al => ThRnFAr|B => BCa|B => TiB|B => TiRnFAr|Ca => CaCa|Ca => PB|Ca => PRnFAr|Ca => SiRnFYFAr|Ca => SiRnMgAr|Ca => SiTh|F => CaF|F => PMg|F => SiAl|H => CRnAlAr|H => CRnFYFYFAr|H => CRnFYMgAr|H => CRnMgYFAr|H => HCa|H => NRnFYFAr|H => NRnMgAr|H => NTh|H => OB|H => ORnFAr|Mg => BF|Mg => TiMg|N => CRnFAr|N => HSi|O => CRnFYFAr|O => CRnMgAr|O => HP|O => NRnFAr|O => OTi|P => CaP|P => PTi|P => SiRnFAr|Si => CaSi|Th => ThCa|Ti => BP|Ti => TiTi|e => HF|e => NAl|e => OMg";
var rules = replacementStr.split("|").map(function(s){
	var parts = s.split(" => ");
	return {from:parts[0], to:parts[1]};
});
var medicine = "CRnSiRnCaPTiMgYCaPTiRnFArSiThFArCaSiThSiThPBCaCaSiRnSiRnTiTiMgArPBCaPMgYPTiRnFArFArCaSiRnBPMgArPRnCaPTiRnFArCaSiThCaCaFArPBCaCaPTiTiRnFArCaSiRnSiAlYSiThRnFArArCaSiRnBFArCaCaSiRnSiThCaCaCaFYCaPTiBCaSiThCaSiThPMgArSiRnCaPBFYCaCaFArCaCaCaCaSiThCaSiRnPRnFArPBSiThPRnFArSiRnMgArCaFYFArCaSiRnSiAlArTiTiTiTiTiTiTiRnPMgArPTiTiTiBSiRnSiAlArTiTiRnPMgArCaFYBPBPTiRnSiRnMgArSiThCaFArCaSiThFArPRnFArCaSiRnTiBSiThSiRnSiAlYCaFArPRnFArSiThCaFArCaCaSiThCaCaCaSiRnPRnCaFArFYPMgArCaPBCaPBSiRnFYPBCaFArCaSiAl";

function doAnyOperation(inp, rules){
	for(var i = 0; i < rules.length; i++){
		var rule = rules[i];
		var ix = inp.indexOf(rule.to);
		if(ix >= 0){
			return inp.substring(0, ix) + rule.from + inp.substring(ix + rule.to.length);
		}
	}
	console.log("no possible replcements");
}

var operations = 0;
while(medicine !== 'e'){
	medicine = doAnyOperation(medicine, rules);
	console.log("len now", medicine.length);
	operations++;
}

console.log(operations);
