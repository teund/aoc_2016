import { Coord, Grid } from "./modules/grid";
import astar = require("astar-algorithm");
const depth = 11541;
const target = new Coord(14,778);
// const depth = 510;
// const target = new Coord(10,10);

const grid = new Grid<string>();
const start = new Coord(0,0);
grid.add(start, "X");
grid.add(target, "T");

const geoIndex = (c: Coord): number =>{
    if(c.name() == "0x0")return 0;
    if(c.name() == target.name())return 0;
    if(c.y === 0) return c.x * 16807;
    if(c.x === 0) return c.y * 48271;
    return erosionLevel(c.above) * erosionLevel(c.left);

}
const elCache: {[k:string]: number} = {};
const erosionLevel = (c: Coord): number =>{
    const cname = c.name();
    if(cname in elCache) return elCache[cname];
    const geoIx = geoIndex(c);
    const el = (geoIx + depth) % 20183;
    elCache[cname] = el;
    return el;
}
const matter = (erLevel: number) => [".", "=", "|"][erLevel % 3];

console.log(grid.toString((c, v) => {
    if(v) return v;
    const lvl = erosionLevel(c);
    return matter(lvl);
}));

enum Tool{Torch, ClimbingGear, Nothing};
class state {
    constructor(
        public pos: Coord, public tool: Tool
    ){}
}
const canUseIn = (m: string, tool: Tool):boolean => {
    if(m == ".") return tool !== Tool.Nothing;
    if(m == "=") return tool !== Tool.Torch;
    if(m == "|") return tool !== Tool.ClimbingGear;
    throw "weird";
}
let callbacks: astarCbs<state> = {
    // It should return id / key / hash for a node
    id: (node) => {
        return `${node.pos.name()} ${node.tool}`;
    },
    // It should check: is a node is the goal?
    isGoal: (node) => {
        return node.pos.name() == target.name() && node.tool == Tool.Torch;
    },
    // It should return an array of successors / neighbors / children
    getSuccessors(node) {
        console.log(`Work from ${node.pos.name()}`);
        const gearChanges = [0,1,2].filter(ng => {
            const m = matter(erosionLevel(node.pos));
            return canUseIn(m, ng);
        }).map(ng => new state(node.pos, ng));
        const steps = node.pos.neighbours()
        .filter(neigh => neigh.x >= 0 && neigh.y >= 0)
        .filter(neigh => {
            const m = matter(erosionLevel(neigh));
            return canUseIn(m, node.tool);
        }).map(c => new state(c, node.tool));
        return gearChanges.concat(steps);
    },
    // g(x). It should return the cost of path between two nodes
    distance: (nodeA, nodeB) => {
      if(nodeA.tool === nodeB.tool)return nodeA.pos.distanceMnhtn(nodeB.pos);
      return 7;
    },
    // h(x). It should return the cost of path from a node to the goal
    estimate(node, goal) {
        return node.pos.distanceMnhtn(goal.pos);
    }
  }
   
  const pathStart = new state(start, Tool.Torch);
  const pathTarget = new state(target, Tool.Torch);
  
  let path = astar<state>(pathStart, pathTarget, callbacks);

  console.log(path.reduce((a,v,i) => {
      if(i == path.length - 1){
          return a;
      }
      return a + callbacks.distance(path[i], path[i+1]);
  }, 0));
