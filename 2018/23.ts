import { promisify } from "util";
import * as fs from "fs";
import { parseToObjects} from "./modules/lineParser";
import { firstBy } from "thenby";
import { Coord, Grid, Direction } from "./modules/grid";
const read = promisify(fs.readFile);

class NanoBot{
    constructor(public x: number, public y: number, public z: number, public r: number){}
    distanceMnhtn(other: NanoBot){
        return Math.abs(this.x-other.x) + Math.abs(this.y-other.y) + Math.abs(this.z-other.z);
    }
}
read("input/23.txt", "utf8").then((d) =>{
//     d = `pos=<0,0,0>, r=4
// pos=<1,0,0>, r=1
// pos=<4,0,0>, r=3
// pos=<0,2,0>, r=1
// pos=<0,5,0>, r=3
// pos=<0,0,3>, r=1
// pos=<1,1,1>, r=1
// pos=<1,1,2>, r=1
// pos=<1,3,1>, r=1`;
    const bots = parseToObjects(d, /pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)/, 
    s => (new NanoBot(Number(s[1]), Number(s[2]), Number(s[3]), Number(s[4]))));

    const strongest = bots.reduce((a,v) => {
        if(a == null || a.r <= v.r)return v;
        return a;
    }, null);
    const inRange = bots.filter(ob => strongest.r >= strongest.distanceMnhtn(ob));
    console.log(inRange.length);
});    