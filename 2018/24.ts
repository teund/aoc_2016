import { promisify } from "util";
import * as fs from "fs";
import { parseToObjectsMultiline, parseToObjects } from "./modules/lineParser";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
import { factoradic } from "js-combinatorics";
const read = promisify(fs.readFile);

interface Group {
    id: number;
    side: string;
    units: number;
    hitPoints: number;
    weak: string[];
    immune: string[]
    damage: number;
    damageType: string;
    initiative: number;
}
var idCount = 0;
const toGroup = (side: string) => (s: string[]) => 
{

    let immune: string[] = [];
    let weak: string[] = [];
    if(s[4]){
        const part1 = (s[5] || "").split(', ').filter(s => s.length);
        if(s[4] === "immune") immune = immune.concat(part1);
        if(s[4] === "weak") weak = weak.concat(part1);
    }
    if(s[8]){
        const part2 = (s[9] || "").split(', ').filter(s => s.length);
        if(s[8] === "immune") immune = immune.concat(part2);
        if(s[8] === "weak") weak = weak.concat(part2);
    }
    return {
        id: idCount++,
        side: side,
        units: Number(s[1]),
        hitPoints: Number(s[2]),
        damage: Number(s[10]),
        damageType: s[11],
        initiative: Number(s[12]),
        weak: weak,
        immune: immune
    }
}
const calcDamage = (att: Group, def: Group) => {
    const factor = (def.weak.indexOf(att.damageType) > -1) ? 2 :
        (def.immune.indexOf(att.damageType) > -1) ? 0 : 1;
    return att.units * att.damage * factor;
}
const inputRe: RegExp = /^(\d+) units each with (\d+) hit points (?:\(((\w+) to ([^;)]+))?(; )?((\w+) to ([^)]+))?\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)/;

read("input/24a.txt", "utf8").then((d) =>{
//     d = `17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
// 989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3`;

    const immuneGroups = parseToObjects<Group>(d, inputRe, toGroup("immune"));
    read("input/24b.txt", "utf8").then((d) =>{

//     d = `801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
// 4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4`;
        const infectionGroups = parseToObjects<Group>(d, inputRe, toGroup("infect"));
        const all = immuneGroups.concat(infectionGroups);

        while(true){
            // select
            const selected: [Group, Group][] = [];
            all.sort(firstBy<Group>(gr => gr.units * gr.damage, -1).thenBy(gr => gr.initiative, -1));
            all.forEach(attacker => {
                    const victims = all
                        .filter(v => v.side !== attacker.side)
                        .filter(v => selected.findIndex(s => s[1].id === v.id) === -1)
                        .filter(vic => calcDamage(attacker, vic) > 0)
                        .sort(
                            firstBy<Group>(vic => calcDamage(attacker, vic), -1)
                                .thenBy(vic => vic.units * vic.damage, -1)
                                .thenBy(gr => gr.initiative, -1)
                    );
                    if(victims.length > 0){
                        selected.push([attacker, victims[0]]);
                    }
                });
            // attack
            for (const attack of selected.sort(firstBy(p => p[0].initiative, -1))) {
                if(attack[0].units > 0){
                    const damage = calcDamage(attack[0], attack[1]);
                    const nrOfUnitsSlayed = Math.floor(damage / attack[1].hitPoints);
                    attack[1].units -= nrOfUnitsSlayed;
                    if(attack[1].units <=0){
                        attack[1].units = 0;
                        all.splice(all.findIndex(gr => gr.id === attack[1].id), 1);
                    }
                }
                
            }
            const remainingImmunes = all.filter(gr => gr.side === "immune");
            const remainingInfect = all.filter(gr => gr.side === "infect");
            if(remainingImmunes.length === 0 || remainingInfect.length === 0) break;
        }
        console.log(all.reduce((a,v) => a+v.units, 0))
    });
});    

