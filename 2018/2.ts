import {promisify} from "util";
import * as fs from "fs";
const read = promisify(fs.readFile);

const hasDuplicates = (s:string, n:number) => {
    const countedChars = s.split("").reduce<{[key:string]:number}>((a, v) => {
        a[v] = (a[v] || 0) + 1;
        return a;
    }, {});
    return Object.keys(countedChars).filter(k => countedChars[k] === n).length > 0;
}

read("input/2.txt", "utf8").then((d) =>{
    const values = d.split("\n").filter(s => s.length);

    const keysWithDoubles = values.filter(s => hasDuplicates(s, 2));
    const keysWithTriples = values.filter(s => hasDuplicates(s, 3));
    console.log(keysWithDoubles.length * keysWithTriples.length);

});
