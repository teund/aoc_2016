import { exists } from "fs";

const seen: {[k: number]: boolean} = {};
const last10: number[] = [];
let v0 = 0;
let v2 = 0;
outer: do{ 
    let v5 = v2 | 0x010000;
    v2 = 16123384
    do{
        let v3 = v5 & 0xFF
        v2 += v3
        v2 &= 0xFFFFFF
        v2 *= 65899
        v2 &= 0xFFFFFF
        if(v5 < 256){
            if(v2 in seen){
                console.log(`Seen ${Object.keys(seen).length} values`);
                console.log(last10.join());
                stop;
            }
            seen[v2] = true;
            last10.push(v2);
            last10.splice(0, last10.length-10);

            if(v2 == v0) 
            {
                stop;
            } else{
                continue outer
            }
        }else{
            v5 = Math.floor(v5/256)
        }
    } while(true)

} while(true)
