import {promisify} from "util";
import * as fs from "fs";
const read = promisify(fs.readFile);
read("input/1.txt", "utf8").then((d) =>{
    const values = d.split("\n").map(Number).filter(n => n!== 0);
    //const values = [+7, +7, -2, -7, -4];

    const seen : {[key:number]: boolean} = {};
    var acc = 0;
    outer:
    while(true){
        for (const diff of values) {
            if(seen[acc]){
                console.log(acc);
                break outer;
            }else{
                seen[acc] = true;
            }
            acc+=diff;
        }
    }

});
