import { promisify } from "util";
import * as fs from "fs";
import { parseToObjectsMultiline, parseToObjects } from "./modules/lineParser";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
const read = promisify(fs.readFile);

interface MachineState{
    registers: number[];
}
interface Sample{
    before: MachineState;
    instruction: number[];
    after: MachineState;
}

const operations: {[k:string]: Function} = {
    "addr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] + reg[args[1]]},
    "addi": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] + args[1]},

    "mulr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] * reg[args[1]]},
    "muli": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] * args[1]},

    "banr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] & reg[args[1]]},
    "bani": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] & args[1]},

    "borr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] | reg[args[1]]},
    "bori": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] | args[1]},

    "setr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]]},
    "seti": (args: number[], reg: number[]) => {reg[args[2]] = args[0]},

    "gtir": (args: number[], reg: number[]) => {reg[args[2]] = (args[0] > reg[args[1]] ? 1 : 0)},
    "gtri": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] > args[1] ? 1 : 0)},
    "gtrr": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] > reg[args[1]] ? 1 : 0)},

    "eqir": (args: number[], reg: number[]) => {reg[args[2]] = (args[0] === reg[args[1]] ? 1 : 0)},
    "eqri": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] === args[1] ? 1 : 0)},
    "eqrr": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] === reg[args[1]] ? 1 : 0)},

};

read("input/16.txt", "utf8").then((d) =>{
    //     d = ``;
        let samples = parseToObjectsMultiline<Sample>(d, /^Before: \[(\d+), (\d+), (\d+), (\d+)]\n(\d+) (\d+) (\d+) (\d+)\nAfter:  \[(\d+), (\d+), (\d+), (\d+)]/, (s) => 
            ({
                before: {registers:[Number(s[1]), Number(s[2]), Number(s[3]), Number(s[4])]},
                after: {registers:[Number(s[9]), Number(s[10]), Number(s[11]), Number(s[12])]},
                instruction:[Number(s[5]), Number(s[6]), Number(s[7]), Number(s[8])]})
            , 4);
        const sampleFits = samples.map(sample => {
            const outputs = Object.keys(operations).map(k => {
                const op = operations[k];
                const reg = sample.before.registers.slice(0);
                op(sample.instruction.slice(1), reg);
                return reg;
            })
            .filter(o => sample.after.registers.every((v,i)=> o[i] === v));
            return outputs.length;
        });
        console.log(`Part 1: ${sampleFits.filter(cnt => cnt >= 3).length}`);

        const useOfIds = Object.keys(operations)
            .reduce<{[k:string]:number}>((a,k) => {
                const op = operations[k];
                var counts = samples.reduce<{[k:number]:number[]}>((a2,s) => {
                    const opCode = s.instruction[0];
                    if(!(opCode in a2)){
                        a2[opCode] = [0,0];
                    }
                    const reg = s.before.registers.slice(0);
                    op(s.instruction.slice(1), reg);
                    a2[opCode][0]++;
                    if(reg.every((v,i) => v === s.after.registers[i])){
                        a2[opCode][1]++;
                    }
                    return a2;
                }, {});
                const perfectMatches = Object.keys(counts).map(Number).filter(id => counts[id][0] === counts[id][1]);
                console.log(`${k}: ${perfectMatches.join(",")}`);

                return a;
            }, {});
        const idForMnemonic: {[k:string]: number} = {
            addr: 1,
            addi: 3,
            mulr: 2,
            muli: 13,
            banr: 5,
            bani: 0,
            borr: 6,
            bori: 10,
            setr: 11,
            seti: 8,
            gtir: 15,
            gtri: 4,
            gtrr: 14,
            eqir: 12,
            eqri: 7,
            eqrr: 9
        };
        const mnemForId = Object.keys(idForMnemonic).reduce<{[k:number]:string}>((a,k)=>{
            a[idForMnemonic[k]] = k;
            return a;
        }, {});
        read("input/16a.txt", "utf8").then((d2) =>{
            let instructions = parseToObjects<number[]>(d2, /^(\d+) (\d+) (\d+) (\d+)/, (s) => [Number(s[1]), Number(s[2]), Number(s[3]), Number(s[4])]);
            const state: MachineState = {registers:[0,0,0,0]};
            for (const inst of instructions) {
                const mnem = mnemForId[inst[0]];
                const op = operations[mnem];
                op(inst.slice(1), state.registers);
            } 
        });
});    