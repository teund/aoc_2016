import { promisify } from "util";
import * as fs from "fs";
import { firstBy } from "thenby";
import { Coord, Grid, Direction } from "./modules/grid";
const read = promisify(fs.readFile);
enum PlaceType{StartRoom, Room, Door}
class Place{
    constructor(public type: PlaceType, public pos: Coord, reachedFrom: Direction, public doorNr: number){}
    toString(){
        if(this.type === PlaceType.StartRoom) return "X";
        if(this.type === PlaceType.Room) return ".";
        if(this.type === PlaceType.Door) return "+";
        return " ";
    }
}

const expandFrom = (pat: string, gr: Grid<Place>, p: Place, stack: Place[]) =>
{
    if(pat.length === 0){
        return;
    }
    const nibble = pat[0];
    const remainPat = pat.substring(1);
    const map: {[k:string]: Direction} = {
        "N": Direction.North,
        "E": Direction.East,
        "S": Direction.South,
        "W": Direction.West
    };
    if(nibble in map){
        const dir = map[nibble];
        const door = p.pos.neighbourTo(dir);
        const nextRoom = door.neighbourTo(dir);
        if(!gr.forCoord(nextRoom)){
            gr.add(door, new Place(PlaceType.Door, door, null, null));
            gr.add(nextRoom, new Place(PlaceType.Room, nextRoom, dir, p.doorNr + 1));
        }
        expandFrom(remainPat, gr, gr.forCoord(nextRoom), stack);
    }else if(nibble === "("){
        stack.push(p)
        expandFrom(remainPat, gr, p, stack);
    }else if(nibble === ")"){
        stack.pop();
        expandFrom(remainPat, gr, p, stack);
    }else if(nibble === "|"){
        const topOfStack = stack[stack.length-1];
        expandFrom(remainPat, gr, topOfStack, stack);
    }
}

var dimensions: Coord[];
read("input/20.txt", "utf8").then((d) =>{
    //d = `^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$`;
    d = d.trim();
    d = d.substring(1, d.length - 1);
    const grid = new Grid<Place>();
    const start = new Place(PlaceType.StartRoom, new Coord(0,0), null, 0);
    grid.set(start.pos, start);
    const stack : Place[] = [];
    expandFrom(d, grid, start, stack);
    console.log(grid.toString());
    const farthest = Array.from(grid.positions()).map(gp => gp.val).reduce((a,v)=> v.doorNr > a.doorNr ? v : a, start);
    console.log(`Farthest room at ${farthest.pos.name()} via ${farthest.doorNr} doors`);

    const allAbove1000 = Array.from(grid.positions()).map(gp => gp.val).filter(p => p.doorNr >= 1000);
    console.log(`Part 2: ${allAbove1000.length} rooms`);

});    