import { promisify } from "util";
import * as fs from "fs";
import { firstBy } from "thenby";
import { Coord, Grid } from "./modules/grid";
const read = promisify(fs.readFile);

const nextGen = (prev: Grid<string>) => {
    const next = new Grid<string>();
    const neighbourCount: {[k: string]: [number, number]} = {};
    for (const trees of prev.positions("|")) {
        for (const n of trees.pos.neigboursDiag()) {
            if(!(n.name() in neighbourCount)){
                neighbourCount[n.name()] = [0, 0];
            }
            neighbourCount[n.name()][0] += 1;
        }
    }
    for (const yard of prev.positions("#")) {
        for (const n of yard.pos.neigboursDiag()) {
            if(!(n.name() in neighbourCount)){
                neighbourCount[n.name()] = [0, 0];
            }
            neighbourCount[n.name()][1] += 1;
        }
    }
    for (const c of Coord.forRectangle(dimensions[0].x, dimensions[1].x,
        dimensions[0].y, dimensions[1].y)) {

        const curr = prev.forCoord(c);
        if(curr === null){
            if(neighbourCount[c.name()] && neighbourCount[c.name()][0] >= 3){
                next.add(c, "|");
            }
        }else if(curr === "|"){
            if(neighbourCount[c.name()] && neighbourCount[c.name()][1] >= 3){
                next.add(c, "#");
            }else{
                next.add(c, "|");
            }
        }else if(curr === "#"){
            if(neighbourCount[c.name()] && neighbourCount[c.name()][0] >= 1
                && neighbourCount[c.name()][1] >= 1){
                next.add(c, "#");
            }
        }
    }
    return next;
}
var dimensions: Coord[];
read("input/18.txt", "utf8").then((d) =>{
//     d = `.#.#...|#.
// .....#|##|
// .|..|...#.
// ..|#.....#
// #.#|||#|#|
// ...#.||...
// .|....|...
// ||...#|.#|
// |.||||..|.
// ...#.|..|.
// `;
    const startGrid = new Grid<string>();
    startGrid.parseFromString(d, {"|": "|", "#":"#"});
    dimensions = startGrid.boundaries();

    let grid = startGrid;

    for (let it = 1; it <= 10; it++) {
        grid = nextGen(grid);
        console.log(`After ${it} minutes:`);
        console.log(grid.toString());
    }
    const nrOfLumber = Array.from(grid.positions("#")).length;
    const nrOfTrees = Array.from(grid.positions("|")).length;
    console.log(`Resource value: ${nrOfLumber*nrOfTrees}`);

});    