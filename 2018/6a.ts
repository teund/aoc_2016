import {promisify} from "util";
import * as fs from "fs";
import { parseToObjects} from "./modules/lineParser";
const read = promisify(fs.readFile);

Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

class Coord{
    x: number;
    y: number;

    constructor(x: number, y: number){this.x = x; this.y = y;};
    name(){
        return `${this.x}x${this.y}`;
    }
}

read("input/6.txt", "utf8").then((d) =>{
//     d = `1, 1
// 1, 6
// 8, 3
// 3, 4
// 5, 5
// 8, 9
// `;
    const points: Coord[] = parseToObjects(d, /^(\d+), (\d+)/, (s: string[]) => 
        (new Coord(Number(s[1]), Number(s[2]))));

    const maxX = points.reduce((a,v)=> Math.max(a, v.x), 0);
    const maxY = points.reduce((a,v)=> Math.max(a, v.y), 0);
    const limit = 10000;
    const dist = (c1: Coord, c2: Coord) =>
    {
        return Math.abs(c1.x - c2.x) + Math.abs(c1.y - c2.y);
    }
    const places = Array.from(new Array(maxX+1),(val,index)=>index)
        .flatMap(x => {
            return Array.from(new Array(maxY+1),(val,index)=>index)
                .map(y => new Coord(x, y));
        });
    const placesWithin = places.filter(place => {
        const summedDist = points.reduce((a,v)=> a + dist(place,v), 0);
        return summedDist < limit;

    });
    console.log(placesWithin.length);
});

