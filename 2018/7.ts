import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
import {uniq, without} from "lodash";
const read = promisify(fs.readFile);


Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

interface Note {from: string, to: string};
class Step{
    waitsFor: string[] = [];
    done = false;
    constructor(public name:string){}
}
const selectNextStep = (steps:{[key:string]:Step})=>{
    const doableSteps = Object.keys(steps).filter(s => (!steps[s].done) && steps[s].waitsFor.length === 0);
    if(doableSteps.length === 0)return '';
    return doableSteps.sort()[0];
}
const completeNextStep = (steps:{[key:string]:Step}, nextStep: string) => {
    Object.keys(steps).forEach(k => {
        steps[k].waitsFor = without(steps[k].waitsFor, nextStep);
    });
    steps[nextStep].done = true;
}
read("input/7.txt", "utf8").then((d) =>{
    const notes: Note[] = parseToObjects(d, /^Step (\w) must be finished before step (\w) can begin/, (s) => 
        ({from: s[1], to: s[2]}));
    const steps = uniq(notes.flatMap(n => [n.from, n.to]))
        .map(n => new Step(n)).reduce<{[key:string]:Step}>((a,v)=> {a[v.name] = v; return a;}, {});
    notes.forEach(note => {
        steps[note.to].waitsFor.push(note.from);
    });
    var path = "";
    while(true){
        const nxt = selectNextStep(steps);
        if(nxt === '')break;
        path = path + nxt;
        completeNextStep(steps, nxt);
    }
    
    console.log(path);

});
