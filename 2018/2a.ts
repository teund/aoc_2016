import {promisify} from "util";
import * as fs from "fs";
import * as combi from "js-combinatorics";
import { firstBy } from "thenby";
const read = promisify(fs.readFile);

const overlap = (first: string, second: string) => {
    const charsFirst = first.split("");
    return charsFirst.filter((c,i) => second[i] === c).join("");
}

read("input/2.txt", "utf8").then((d) =>{
    const values = d.split("\n").filter(s => s.length);
    const combinations = combi.bigCombination(values, 2).toArray();
    const overlaps = combinations.map(pair => overlap(pair[0], pair[1]));
    console.log(overlaps.sort(firstBy(s => s.length, {direction:-1}))[0]);

});
