import { promisify } from "util";
import * as fs from "fs";
import { parseToObjectsMultiline, parseToObjects } from "./modules/lineParser";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
import { factoradic } from "js-combinatorics";
const read = promisify(fs.readFile);

interface Point {
    id: number;
    constellation: string;
    coords: number[];
}
const close = (p1: Point, p2: Point) =>{
    return p1.coords.reduce((a,v,i) => a + Math.abs(v - p2.coords[i]), 0) <= 3;
}
read("input/25.txt", "utf8").then((d) =>{
//     d = `1,-1,-1,-2
// -2,-2,0,1
// 0,2,1,3
// -2,3,-2,1
// 0,2,3,-2
// -1,-1,1,-2
// 0,-2,-1,0
// -2,2,3,-1
// 1,2,2,0
// -1,-2,0,-2`;

    const points = parseToObjects<Point>(d, /((-?\d+)(,(-?\d+))*)/, 
        (s, i) => ({id: i, constellation: null, coords:s[1].split(',').map(Number)}));
    for (let i1 = 0; i1 < points.length; i1++) {
        const point1 = points[i1];
        if (!point1.constellation) point1.constellation = "c" + point1.id;
        for (let i2 = i1+1; i2 < points.length; i2++) {
            const point2 = points[i2];
            if(close(point1, point2)){
                if(!(point2.constellation)){
                    point2.constellation = point1.constellation;
                }else if(point2.constellation !== point1.constellation){
                    // point1's constellation converted to point 2's
                    const changeFrom = point1.constellation;
                    const changeTo = point2.constellation;
                    console.log(`Converting all ${changeFrom} to ${changeTo}`);
                    for (let i3 = 0; i3 < points.length; i3++) {
                        const point3 = points[i3];
                        if(point3.constellation === changeFrom){
                            point3.constellation = changeTo;
                        }
                    }
                }
            }
        }
    }
    const constellations = points.reduce<{[k:string]: boolean}>((a,v) => {
        a[v.constellation] = true;
        return a;
    }, {}); 
    console.log(`Number of constellations: ${Object.keys(constellations).length}`);


});    

