import { promisify } from "util";
import * as fs from "fs";
import { parseToObjectsMultiline, parseToObjects } from "./modules/lineParser";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
import { factoradic } from "js-combinatorics";

interface Group {
    id: number;
    side: string;
    units: number;
    hitPoints: number;
    weak: string[];
    immune: string[]
    damage: number;
    damageType: string;
    initiative: number;
}
var idCount = 0;
const toGroup = (side: string, boost: number) => (s: string[]) => 
{

    let immune: string[] = [];
    let weak: string[] = [];
    if(s[4]){
        const part1 = (s[5] || "").split(', ').filter(s => s.length);
        if(s[4] === "immune") immune = immune.concat(part1);
        if(s[4] === "weak") weak = weak.concat(part1);
    }
    if(s[8]){
        const part2 = (s[9] || "").split(', ').filter(s => s.length);
        if(s[8] === "immune") immune = immune.concat(part2);
        if(s[8] === "weak") weak = weak.concat(part2);
    }
    return {
        id: idCount++,
        side: side,
        units: Number(s[1]),
        hitPoints: Number(s[2]),
        damage: Number(s[10]) + boost,
        damageType: s[11],
        initiative: Number(s[12]),
        weak: weak,
        immune: immune
    }
}
const calcDamage = (att: Group, def: Group) => {
    const factor = (def.weak.indexOf(att.damageType) > -1) ? 2 :
        (def.immune.indexOf(att.damageType) > -1) ? 0 : 1;
    return att.units * att.damage * factor;
}
const inputRe: RegExp = /^(\d+) units each with (\d+) hit points (?:\(((\w+) to ([^;)]+))?(; )?((\w+) to ([^)]+))?\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)/;

const battle = (boost: number) => {
    let d = fs.readFileSync("input/24a.txt", "utf8")
    //     d = `17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
    // 989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3`;
    
    const immuneGroups = parseToObjects<Group>(d, inputRe, toGroup("immune", boost));
    d = fs.readFileSync("input/24b.txt", "utf8");
            
    //     d = `801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
    // 4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4`;
    const infectionGroups = parseToObjects<Group>(d, inputRe, toGroup("infect", 0));
    
    const all = immuneGroups.concat(infectionGroups);
    var prevUnits: number = null;

    while(true){
        // select
        const selected: [Group, Group][] = [];
        all.sort(firstBy<Group>(gr => gr.units * gr.damage, -1).thenBy(gr => gr.initiative, -1));
        all.forEach(attacker => {
                const victims = all
                    .filter(v => v.side !== attacker.side)
                    .filter(v => selected.findIndex(s => s[1].id === v.id) === -1)
                    .filter(vic => calcDamage(attacker, vic) > 0)
                    .sort(
                        firstBy<Group>(vic => calcDamage(attacker, vic), -1)
                            .thenBy(vic => vic.units * vic.damage, -1)
                            .thenBy(gr => gr.initiative, -1)
                );
                if(victims.length > 0){
                    selected.push([attacker, victims[0]]);
                }
            });
        // attack
        for (const attack of selected.sort(firstBy(p => p[0].initiative, -1))) {
            if(attack[0].units > 0){
                const damage = calcDamage(attack[0], attack[1]);
                const nrOfUnitsSlayed = Math.floor(damage / attack[1].hitPoints);
                attack[1].units -= nrOfUnitsSlayed;
                if(attack[1].units <=0){
                    attack[1].units = 0;
                    all.splice(all.findIndex(gr => gr.id === attack[1].id), 1);
                }
            }
            
        }
        const remainingUnits = all.reduce((a,v) => a+v.units, 0);
        const remainingImmunes = all.filter(gr => gr.side === "immune");
        if(remainingImmunes.length === 0)    return [remainingUnits, "infect"];

        const remainingInfect = all.filter(gr => gr.side === "infect");
        if(remainingInfect.length === 0)    return [remainingUnits, "immune"];

        if(prevUnits === remainingUnits) {
            return [remainingUnits, "undecided"];

        }
        prevUnits = remainingUnits;
    }
    
}
var boost = 0;
while(true){
    console.log(boost);
    const result = battle(boost);
    console.log(result);
    if(result[1] === "immune")break;
    boost++;
}
