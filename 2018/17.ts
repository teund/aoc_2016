import { promisify } from "util";
import * as fs from "fs";
import { parseToObjects } from "./modules/lineParser";
import {Coord, Grid} from "./modules/grid";
import { start } from "repl";
const read = promisify(fs.readFile);

Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};
const findToRightWhereNot = (from: Coord, whereNot: string, gr: Grid<string>) =>{
    let pos = from;
    while(true){
        pos = pos.right;
        if(gr.forCoord(pos) !== whereNot){
            return pos;
        }
    }
}
const nextGrid = (gr: Grid<string>, toEval: Coord[]) => {
    const result = gr;//.clone();
    const evaluate = toEval.splice(0, toEval.length);
    for (const streaming of evaluate.filter(c => gr.forCoord(c) === "|")) {
        const below = gr.forCoord(streaming.below);
        if(below === null){
            result.add(streaming.below, "|")
            console.log(`Drip, dip: ${streaming.below.y}`);
            toEval.push(streaming.below)
        }
        if(below === "#" || below === "~"){
            const flowSideways = (g:Grid<string>, c: Coord, step: number): Coord => {
                const neighbour = c.offset(step, 0);
                if(gr.forCoord(neighbour) === "#"){
                    return neighbour;
                }
                gr.setIfEmpty(neighbour, "|");
                if(gr.forCoord(neighbour.below) === "#" || gr.forCoord(neighbour.below) === "~"){
                    return flowSideways(g, neighbour, step);
                } else{
                    toEval.push(neighbour);
                    return neighbour;
                }
            }
            const toLeft = flowSideways(gr, streaming, -1);
            const toRight = flowSideways(gr, streaming, 1);
            if(gr.forCoord(toLeft) === "#" && gr.forCoord(toRight) === "#"){
                for (let x = toLeft.x+1; x < toRight.x; x++) {
                    const newStandingWater = new Coord(x, toLeft.y);
                    result.set(newStandingWater, "~");
                    toEval.push(newStandingWater.above);
                }
            }
        }
    }
    return result;
}


read("input/17.txt", "utf8").then((d) =>{
//     d = `x=495, y=2..7
// y=7, x=495..501
// x=501, y=3..7
// x=498, y=2..4
// x=506, y=1..2
// x=498, y=10..13
// x=504, y=10..13
// y=13, x=498..504`;
    const coordinates = parseToObjects<Coord[]>(d, /(x|y)=(\d+), (x|y)=(\d+)\.\.(\d+)/, 
    (s)=>{
        const result: Coord[] = [];
        for (let i = Number(s[4]); i <= Number(s[5]); i++) {
            if(s[1] === "x"){
                result.push(new Coord(Number(s[2]), i));
            }else{
                result.push(new Coord(i, Number(s[2])));
            }
        }
        return result;
    }).flatMap(c => c);
    const startGrid = new Grid<string>();
    coordinates.forEach(c => {
        startGrid.set(c, "#");
    });
    const bound = startGrid.boundaries();
    bound[0].x--;
    bound[1].x++;

    // leave room for the well, we will clip this at the end
    bound[0].y -= 10;

    startGrid.add(new Coord(500, 0), "+");
    startGrid.add(new Coord(500, 1), "|");

    let grid = startGrid;
    let lastHash = startGrid.hash();
    const toEval = [new Coord(500, 1)];
    while(true) {
        grid = nextGrid(grid, toEval);
        grid.clip(bound);
        const newHash = grid.hash();
        if(newHash === lastHash){
            break;
        }else{
            lastHash = newHash;
        }
    }
    // leave room for the well, we will clip this at the end
    bound[0].y += 10;
    grid.clip(bound);

    const standing = Array.from(grid.positions("~"));
    const flowing = Array.from(grid.positions("|"));

    console.log(`Total ${standing.length} standing + ${flowing.length} flowing == ${standing.length + flowing.length}`);
    fs.writeFileSync("output/17.end.txt", grid.toString());

});    
