import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
import {uniq, without} from "lodash";
const read = promisify(fs.readFile);


Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

interface Note {from: string, to: string};
interface Worker {name:string, workingOn: string, done:number};
class Step{
    waitsFor: string[] = [];
    done = false;
    open = true;
    constructor(public name:string){}
}
const selectNextStep = (steps:{[key:string]:Step})=>{
    const doableSteps = Object.keys(steps).filter(s => (!steps[s].done && steps[s].open) && steps[s].waitsFor.length === 0);
    if(doableSteps.length === 0)return '';
    return doableSteps.sort()[0];
}
const completeStep = (steps:{[key:string]:Step}, step: string) => {
    Object.keys(steps).forEach(k => {
        steps[k].waitsFor = without(steps[k].waitsFor, step);
    });
    steps[step].done = true;
}
const getAvailableWorker = (workerQueue: Worker[], time: number) => {
    const available = workerQueue.filter(w => w.done <= time);
    //prefer taking a worker that is currently working on something. It may free things up.
    return available.length ? available.sort(firstBy(w => w.workingOn == null))[0] : null;
}
const timeFor = (task:string) => {
    return task.charCodeAt(0) - 64 + 60;
}

read("input/7.txt", "utf8").then((d) =>{
    const notes: Note[] = parseToObjects(d, /^Step (\w) must be finished before step (\w) can begin/, (s) => 
        ({from: s[1], to: s[2]}));
    const steps = uniq(notes.flatMap(n => [n.from, n.to]))
        .map(n => new Step(n)).reduce<{[key:string]:Step}>((a,v)=> {a[v.name] = v; return a;}, {});
    notes.forEach(note => {
        steps[note.to].waitsFor.push(note.from);
    });
    var path = "";
    var time = 0;
    var workerQueue: Worker[] = [
        {name: "1", workingOn: null, done: 0},
        {name: "2", workingOn: null, done: 0},
        {name: "3", workingOn: null, done: 0},
        {name: "4", workingOn: null, done: 0},
        {name: "5", workingOn: null, done: 0},
    ];
    while(true){
        const worker = getAvailableWorker(workerQueue, time);
        if(worker == null){
            //wait
            const nextDone = workerQueue.sort(firstBy(w => w.done))[0];
            time = nextDone.done;
        }else{
            if(worker.workingOn){
                completeStep(steps, worker.workingOn);
                worker.workingOn = null;
            }
            const nxt = selectNextStep(steps);
            if(nxt === ''){
                if(path.length === Object.keys(steps).length){
                    break;
                }else{
                    time += 1;
                    continue;
                }
            }
            path = path + nxt;
            worker.workingOn = nxt;
            steps[nxt].open = false;
            worker.done = time + timeFor(nxt);
        }
    }
    
    console.log(workerQueue.sort(firstBy(w => w.done, -1))[0].done);

});
