seti 123 0 2        v2 = 123 <- start
bani 2 456 2        v2 = v2 & 456
eqri 2 72 2         if(v2 == 72)
addr 2 4 4              goto label1
seti 0 0 4          goto start

v2 = 123
do{
    v2 &= 456
}until v2 == 72

seti 0 1 2          v2 = 0    <- label1
bori 2 65536 5      v5 = v2 | 65536 (0x010000) <- label10
seti 16123384 4 2   v2 = 16123384
bani 5 255 3        v3 = v5 & 255 (0xFF)  <- label9
addr 2 3 2          v2 += v3
bani 2 16777215 2   v2 &= 16777215 (0xFFFFFF)
muli 2 65899 2      v2 = v2 * 65899
bani 2 16777215 2   v2 &= 16777215 (0xFFFFFF)
gtir 256 5 3        if(256 > v5)
addr 3 4 4              goto label2
addi 4 1 4          goto label3
seti 27 6 4         goto label4 <- label2
seti 0 3 3          v3 = 0      <- label3
addi 3 1 1          v1 = v3 +1        <- label8
muli 1 256 1        v1 = v1 * 256
gtrr 1 5 1          if(v1 > v5)
addr 1 4 4              goto label5
addi 4 1 4          goto label6
seti 25 6 4         goto label7 <- label5
addi 3 1 3          v3++ <- label6
seti 17 3 4         goto label8
setr 3 8 5          v5 = v3 <- label7
seti 7 2 4          goto label9
eqrr 2 0 3          if(v2 == v0)    <- label4
addr 3 4 4              if(v3 > 0) exit
seti 5 3 4          goto label10

v2 = 0
do{ // label10
    v5 = v2 | 0x010000
    v2 = 16123384
    do{
        v3 = v5 & 0xFF
        v2 += v3
        v2 &= 0xFFFFFF
        v2 *= 65899
        v2 &= 0xFFFFFF
        if(v5 < 256){
            if(v2 == v0) exit
        }else{
            v3 = 0
            do{
                except 1st loop: v3++
                v1 = v3 + 1
                v1 = v1 * 256
            }until(v1 > v5)
            v5 = v3

        }
    }

}
