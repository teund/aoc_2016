// const serial = 57;
const serial = 3463;

const fuelLevel = (x: number, y: number) => {
    const rackId = x + 10;
    const l = (rackId*y + serial) * rackId;
    return Math.floor((l % 1000)/100) - 5;
}
const summedLevel = (x: number, y: number, size: number) => {
    var sum = 0;
    for (let dx = 0; dx < size; dx++) {
        for (let dy = 0; dy < size; dy++) {
            sum += fuelLevel(x+dx, y+dy);
        }
    }
    return sum;
}
const deltaSummedLevel = (x: number, y: number, size: number) => {
    var sum = 0;
    for (let dx = 0; dx < size; dx++) {
        sum+=fuelLevel(x+dx, y+size-1);
    }
    for (let dy = 0; dy < size-1; dy++) {
        sum+=fuelLevel(x+size-1, y+dy);
    }
    return sum;
}

const bestOfSize3 = ()=>{
    var best = {x:0, y:0, v:0};
    for (let x = 1; x <= 300-2; x++) {
        for (let y = 1; y <= 300-2; y++) {
            const level = summedLevel(x, y, 3);
            if(level > best.v){
                best = {x:x, y:y, v:level}
            }
        }
    }
    return best;
}
const bestOfAnySize = () => {
    var best = { x: 0, y: 0, size: 0, v: 0 };
    for (let x = 1; x <= 300 - 2; x++) {
        for (let y = 1; y <= 300 - 2; y++) {
            var level = 0;
            for (let size = 1; size <= 300 - x - 1; size++) {
                level += deltaSummedLevel(x, y, size);
                if (level > best.v) {
                    best = { x: x, y: y, size: size, v: level }
                }
            }
        }
    }
    return best;
}
const bestPart1 = bestOfSize3();
console.log(`Highest value ${bestPart1.v} at ${bestPart1.x},${bestPart1.y}`);

const bestPart2 = bestOfAnySize();
console.log(`Highest value ${bestPart2.v} at ${bestPart2.x},${bestPart2.y},${bestPart2.size}`);
