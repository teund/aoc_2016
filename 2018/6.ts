import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { groupBy, uniqWith } from "lodash";
import { parseToObjects} from "./modules/lineParser";
const read = promisify(fs.readFile);

Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

class Coord{
    x: number;
    y: number;

    constructor(x: number, y: number){this.x = x; this.y = y;};
    name(){
        return `${this.x}x${this.y}`;
    }
    neigbours(){
        return [new Coord(this.x-1, this.y), new Coord(this.x+1, this.y),
                new Coord(this.x, this.y -1), new Coord(this.x, this.y + 1)];
    }
}
class Area{
    name: string;
    points: {[key:string]: Coord} = {};
    size(){return Object.keys(this.points).length;}
    dead = false;
    acceleration: string = '=';
    lastGrowth: number = 0;
    expand(){
        if(this.dead) return [];
        const allNeighbours = Object.keys(this.points).flatMap(key => this.points[key].neigbours()).filter(n => !this.points[n.name()]);
        return uniqWith(allNeighbours, (c1, c2) => c1.name() === c2.name() );
    };
    addCells(cc: Coord[]){
        if(cc.length === 0) this.dead = true;
        if(this.lastGrowth > cc.length) this.acceleration = '-';
        if(this.lastGrowth < cc.length) this.acceleration = '+';
        if(this.lastGrowth === cc.length) this.acceleration = '=';
        cc.forEach(c => { this.points[c.name()] = c;});
        this.lastGrowth = cc.length;
    }

}
const displayCells = (x: number, y: number, a: Area[]) => {
    const chars: string[] = Array.apply(null, Array(y*(x+1))).map(function(){return ' '});
    for (let index = 0; index < y; index++) {
        chars[(index)*(x+1)+ x] = '\n';
    }
    a.forEach(area => Object.keys(area.points).forEach(k => {
        const p = area.points[k];
        if(p.x < 0 || p.x >= x || p.y < 0 || p.y >= y)return;
        chars[p.x + (p.y*(x+1))] = area.name.toString()[0];
    }));
    console.log(chars.join(''));
}

read("input/6.txt", "utf8").then((d) =>{
//     d = `1, 1
// 1, 6
// 8, 3
// 3, 4
// 5, 5
// 8, 9
// `;
    const points: Coord[] = parseToObjects(d, /^(\d+), (\d+)/, (s: string[]) => 
        (new Coord(Number(s[1]), Number(s[2]))));
    const areas = points.map((a, i) => {
        const area = new Area();
        area.name = String.fromCharCode(65 + i);
        area.points[a.name()] = a;
        return area;
    });
    const totalArea = points.reduce<{[key:string]:string}>((a, v, i) => {a[v.name()] = String.fromCharCode(65+i); return a;}, {});

    for (let gen = 0; gen < 500; gen++) {
        // get expansion for all areas (remove duplicates)
        const expansions = areas.map(a => a.expand().filter(c => !totalArea[c.name()]))
        const groupedClaims = groupBy(expansions.flatMap(coords => coords),
            (c: Coord) => c.name());
        const duplicateClaims = Object.keys(groupedClaims).filter(k => groupedClaims[k].length > 1);
        duplicateClaims.forEach(k => {
            totalArea[k] = "-";
        });
        areas.forEach((area, i) =>{
            const expansion = expansions[i].filter(c => duplicateClaims.indexOf(c.name()) === -1);
            area.addCells(expansion);
            expansion.forEach(c=>{totalArea[c.name()] = area.name;})
        });
        //displayCells(10,10,areas);
        //console.log(`After generation ${gen}:`);
        areas.sort(firstBy(a => a.size(), -1)).filter((v,i)=> i<40).forEach(area => {
            console.log(`Gen ${gen}: Area ${area.name}: ${area.size()} points ${area.acceleration} ${area.dead?'#':''}`);
        });

    }

    });

