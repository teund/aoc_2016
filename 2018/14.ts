import { promisify } from "util";
import * as fs from "fs";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
const read = promisify(fs.readFile);

const input = 430971;

class State {
    public recipes: string[] = new Array<string>(1000000);
    recipeNr: number = 0;
    elf1Current: number;
    elf2Current: number;
    work(){
        const sum = Number(this.recipes[this.elf1Current]) + Number(this.recipes[this.elf2Current]);
        for (const c of sum.toString().split('')) {
            this.recipes[this.recipeNr] = c;
            this.recipeNr++;
        }
        this.elf1Current += (Number(this.recipes[this.elf1Current]) + 1);
        this.elf2Current += (Number(this.recipes[this.elf2Current]) + 1);
        this.elf1Current = this.elf1Current % this.recipeNr;
        this.elf2Current = this.elf2Current % this.recipeNr;
    }
}

const state = new State();
state.recipes[0] = "3";
state.recipes[1] = "7";
state.recipeNr = 2;
state.elf1Current = 0;
state.elf2Current = 1;
const improvesAfter = input;
while(state.recipeNr < improvesAfter + 10){
    state.work();
}
console.log(state.recipes.slice(improvesAfter, improvesAfter + 10).join(""));