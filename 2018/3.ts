import {promisify} from "util";
import * as fs from "fs";
import { parseToObjects} from "./modules/lineParser";
const read = promisify(fs.readFile);

const overlap = (first: string, second: string) => {
    const charsFirst = first.split("");
    return charsFirst.filter((c,i) => second[i] === c).join("");
}


read("input/3.txt", "utf8").then((d) =>{
    const claims = parseToObjects(d, /^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/, (s: string[]) => 
        ({id: s[1], left: Number(s[2]), top: Number(s[3]), width: Number(s[4]), height: Number(s[5])}));
    const inUse: {[key:string]: string[]} = {};
    const nonOverlapping: {[key:string]: boolean} = {};
    var doubleUsed = 0;
    for (const claim of claims) {
        nonOverlapping[claim.id] = true;
        for (let x = claim.left; x < claim.left + claim.width; x++) {
            for (let y = claim.top; y < claim.top + claim.height; y++) {
                const key = `${x}x${y}`;
                if(inUse[key]){
                    inUse[key].push(claim.id);
                    if(inUse[key].length === 2) doubleUsed++;
                    delete nonOverlapping[claim.id];
                    delete nonOverlapping[inUse[key][0]];
                }else{
                    inUse[key] = [claim.id];
                }
            }
        }
    }
    console.log(`doubleUsed: ${doubleUsed}`);;
    console.log(`no overlap: ${Object.keys(nonOverlapping).join()}`);;

});
