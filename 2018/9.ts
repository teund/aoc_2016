import {promisify} from "util";
import * as fs from "fs";

// const players = 9;
// const lastMarble = 25;
const players = 428;
const lastMarble = 72061;
interface State {
    marbles: number[];
    current: number;
    players: number[];
};
const state: State = {
    marbles: [0],
    current: 0,
    players: Array.from({length:players}, ()=> 0)
};

const circlePos = (i: number, size: number) => {
    return (i + size) % size;
};
const placeMarble = (s: State, m: number, player: number) => {
    if(m % 23 === 0){
        s.players[player] += m;
        const replacePos = circlePos(s.current - 7, s.marbles.length);
        s.players[player] += s.marbles.splice(replacePos, 1)[0];
        s.current = replacePos;
    }else{
        var newPos = circlePos(s.current + 2, s.marbles.length);
        if(newPos === 0)newPos = s.marbles.length;
        s.marbles.splice(newPos, 0, m);
        s.current = newPos;
    }
}
var currPlayer = 0;
for (let m = 1; m <= lastMarble; m++) {
    if(m%10000 === 0){
        console.log(`At ${m} of ${lastMarble}`);
    }
    placeMarble(state, m, currPlayer);
    currPlayer = (currPlayer + 1) % players;
}
console.log(state.players.reduce((a,v)=> {return a>v?a:v;}, 0));