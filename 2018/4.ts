import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
const read = promisify(fs.readFile);

interface Note {time: string, min: number, remark: string, guard: string};

read("input/4.txt", "utf8").then((d) =>{
    const notes: Note[] = parseToObjects(d, /^\[(\d\d\d\d-\d\d-\d\d \d\d:(\d\d))\] (falls asleep|wakes up|Guard #(\d+) begins shift)/, (s: string[]) => 
        ({time: s[1], min: Number(s[2]), remark: s[3], guard: s[4]}));
    const sortedNotes = notes.sort(firstBy("time"));
    var byGuards: {
        [key:string]: {
            guard:string, 
            notes: Note[], 
            sleepMinutes: {[key:number]:number}, 
            sleepMinutesTotal: number}
        } = {};
    var curr = null;
    for (const note of sortedNotes) {
        if(note.guard){
            curr = note.guard;
            if(!byGuards[curr]){
                byGuards[curr] = {guard: note.guard, notes:[], sleepMinutes: {}, sleepMinutesTotal: 0};
            }
        }else{
            byGuards[curr].notes.push(note);
        }
    }
    for (const key of Object.keys(byGuards)) {
        const guard = byGuards[key];
        for (let i = 0; i < guard.notes.length; i += 2) {
            const startNote = guard.notes[i];
            const endNote = guard.notes[i+1];
            for (let min = startNote.min; min < endNote.min; min++) {
                guard.sleepMinutesTotal += 1;
                guard.sleepMinutes[min] = (guard.sleepMinutes[min] || 0) + 1;
            }
        }
    }
    const maxSleepMinute = (sm: {[key:number]:number}) => Object.keys(sm).sort(firstBy(k => sm[Number(k)], -1)).map(m => Number(m))[0];

    const sleepiestGuard = Object.keys(byGuards).map(k => byGuards[k])
        .sort(firstBy(g => g.sleepMinutesTotal, -1))[0];
    const sleepiestMinute = maxSleepMinute(sleepiestGuard.sleepMinutes);
    console.log(`sleepiest guard: ${sleepiestGuard.guard}, ${sleepiestGuard.sleepMinutesTotal} min. Sleepiest minute = ${sleepiestMinute}`);
    console.log(`guard ID x SleepiestMinute: ${Number(sleepiestGuard.guard) * sleepiestMinute}`);

    const sleepiestGuardMinute = Object.keys(byGuards).map(k => {
        const guard = byGuards[k];
        const minute = maxSleepMinute(guard.sleepMinutes);
        return {guard: guard.guard, minute, score: guard.sleepMinutes[minute]};
    }).sort(firstBy("score", -1))[0];
    console.log(`guard ID x SleepiestMinute: ${Number(sleepiestGuardMinute.guard) * sleepiestGuardMinute.minute}`);


});
