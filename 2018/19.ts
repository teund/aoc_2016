import { promisify } from "util";
import * as fs from "fs";
import { parseToObjectsMultiline, parseToObjects } from "./modules/lineParser";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
const read = promisify(fs.readFile);

interface MachineState{
    instructionPointRef: number;
    registers: number[];
}
interface Instruction{
    mnemonic: string;
    args: number[];
}
const operations: {[k:string]: Function} = {
    "addr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] + reg[args[1]]},
    "addi": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] + args[1]},

    "mulr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] * reg[args[1]]},
    "muli": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] * args[1]},

    "banr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] & reg[args[1]]},
    "bani": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] & args[1]},

    "borr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] | reg[args[1]]},
    "bori": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]] | args[1]},

    "setr": (args: number[], reg: number[]) => {reg[args[2]] = reg[args[0]]},
    "seti": (args: number[], reg: number[]) => {reg[args[2]] = args[0]},

    "gtir": (args: number[], reg: number[]) => {reg[args[2]] = (args[0] > reg[args[1]] ? 1 : 0)},
    "gtri": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] > args[1] ? 1 : 0)},
    "gtrr": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] > reg[args[1]] ? 1 : 0)},

    "eqir": (args: number[], reg: number[]) => {reg[args[2]] = (args[0] === reg[args[1]] ? 1 : 0)},
    "eqri": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] === args[1] ? 1 : 0)},
    "eqrr": (args: number[], reg: number[]) => {reg[args[2]] = (reg[args[0]] === reg[args[1]] ? 1 : 0)},

};
const ip = 5;
read("input/19.txt", "utf8").then((d) =>{

//      d = `seti 5 0 1
// seti 6 0 2
// addi 0 1 0
// addr 1 2 3
// setr 1 0 0
// seti 8 0 4
// seti 9 0 5`;
        const instructions = parseToObjects<Instruction>(d, /^(\w+) (\d+) (\d+) (\d+)/, (s) => 
            ({mnemonic: s[1], args: [Number(s[2]), Number(s[3]), Number(s[4])]}));
        const state: MachineState = { instructionPointRef:ip, registers:[0,0,0,0,0,0]};
        while(true){
            const inst = instructions[state.registers[state.instructionPointRef]];
            if(!inst)break;
            const op = operations[inst.mnemonic];
            op(inst.args, state.registers);
            state.registers[state.instructionPointRef]++;
        }
        console.log(state.registers[0]);
});    
