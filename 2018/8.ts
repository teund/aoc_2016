import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
import {uniq, without} from "lodash";
import { Z_FILTERED } from "zlib";
const read = promisify(fs.readFile);


Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

class Node{
    constructor(){}
    metadata: number[] =[];
    children: Node[] = [];
    readFromStream(data : number[], after: number):number{
        const nrOfChildren = data[after+1];
        const nrOfMetaData = data[after+2];
        var readChildAfter = after + 2;
        this.children = Array.from({length:nrOfChildren}, (v,k)=> new Node());
        const endPoint = this.children.reduce((a,v)=>{
            const end = v.readFromStream(data, a);
            return end;
        }, readChildAfter);
        this.metadata = data.slice(endPoint + 1, endPoint + nrOfMetaData + 1)
        return endPoint + nrOfMetaData;
    };
    allNodes():Node[]{
        const allDesc = this.children.flatMap(c => c.allNodes());
        return [this, ...allDesc];
    };
    value(): number{
        if(this.children.length === 0){
            return this.metadata.reduce((a,v) => a+v, 0);
        }else{
            return this.metadata.map(m => this.children[m-1])
                .filter(Boolean)
                .reduce((a,v) => a+v.value(), 0);
        }
    }
}
read("input/8.txt", "utf8").then((d) =>{
    //d = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";
    const data = d.split(" ").map(Number);
    const root = new Node();
    root.readFromStream(data, -1);

    console.log(
        root.allNodes().flatMap(n => n.metadata).reduce((a,v) => a+v,0)
    );
    console.log(root.value());

});
