import { parseToObjects} from "./modules/lineParser";

// const initialState = "##...#......##......#.####.##.#..#..####.#.######.##..#.####...##....#.#.####.####.#..#.######.##...";

// const rulesInput = `#.... => .
// #..## => #
// ....# => .
// ...#. => .
// ...## => #
// #.#.# => .
// .#... => #
// ##.#. => .
// ..#.# => .
// .##.# => #
// ###.# => #
// .#.## => .
// ..... => .
// ##### => #
// ###.. => .
// ##..# => #
// #.### => #
// #.#.. => .
// ..### => .
// ..#.. => .
// .#..# => #
// .##.. => #
// ##... => #
// .#.#. => #
// .###. => #
// #..#. => .
// ####. => .
// .#### => #
// #.##. => #
// ##.## => .
// ..##. => .
// #...# => #`;
const initialState = "#.#.#...#..##..###.##.#...#.##.#....#..#.#....##.#.##...###.#...#######.....##.###.####.#....#.#..##";

const rulesInput = `#...# => #
....# => .
##..# => #
.#.## => #
##.## => .
###.# => #
..... => .
...#. => .
.#.#. => #
#.##. => #
..#.# => #
.#... => #
#.#.. => .
##.#. => .
.##.. => #
#..#. => .
.###. => .
..#.. => .
#.### => .
..##. => .
.#..# => #
.##.# => .
.#### => .
...## => #
#.#.# => #
..### => .
#..## => .
####. => #
##### => .
###.. => #
##... => #
#.... => .`;

interface Rule {
    result: boolean;
    current: boolean[];
}
interface State {
    pots: boolean[];
    offset: number;
}
const state: State = {pots: initialState.split('').map(c => c === '#'), offset:0};
var rules: Rule[] = parseToObjects(rulesInput, /^([#.])([#.])([#.])([#.])([#.]) => ([#.])/, (s) => 
({result: (s[6] === '#'), current: [(s[1] === '#'),(s[2] === '#'),(s[3] === '#'),(s[4] === '#'),(s[5] === '#')]}));

const nextGen = (gen: State, rules: Rule[]) => {
    let result: boolean[] = [];
    const appended = [false, false, false, false, ...gen.pots, false, false, false, false];
    for (let index = 0; index < appended.length-4; index++) {
        result[index] = matchResult(appended.slice(index, index + 5), rules);
    }
    return {pots: result, offset: gen.offset - 2};
}
const strip = (gen: State) => {
    const firstPlant = gen.pots.indexOf(true);
    const lastPlant = gen.pots.lastIndexOf(true);
    return {pots: gen.pots.slice(firstPlant, lastPlant + 1), offset: gen.offset + firstPlant};
}
const keyFor = (s: State) => {
    return s.pots.map(b => b?'#':'.').join('');
}
const matchResult = (snip: boolean[], rules: Rule[])=>{
    for (const rule of rules) {
        if(snip[0] === rule.current[0]
            && snip[1] === rule.current[1]
            && snip[2] === rule.current[2]
            && snip[3] === rule.current[3]
            && snip[4] === rule.current[4]
            ){
                return rule.result;
        }
    }
    return false;
}
const knownStates: {[key: string]:number[]} = {};
let currState: State = state;
const gens = 50000000000;
for (let index = 0; index < gens; index++) {
    currState = nextGen(currState, rules);
    currState = strip(currState);
    const key = keyFor(currState);
    if(key in knownStates){
        const prevState = knownStates[key];
        const loopSize = index - prevState[0];
        const dOffset = currState.offset - prevState[1];
        const times = Math.floor((gens - 1 - index)/loopSize);
        currState.offset += times * dOffset;
        index += times * loopSize;
    }else{
        knownStates[key] = [index, currState.offset];
    }
}
console.log(`${keyFor(currState)}`);
console.log(`${currState.pots.reduce((a,v,i) => {
    if(v){
        return a + (i + currState.offset);
    }
    return a;
}, 0)}`);

