import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
const read = promisify(fs.readFile);

interface Note {time: string, min: number, remark: string, guard: string};
const reactive = (c1:string, c2:string) =>{
    return c1 !== c2 && c1.toLowerCase() === c2.toLowerCase();
}
const react = (l:string[]) => {
    var list = l.slice(0);
    while(true){
        // mark reactions
        for (let index = 0; index < list.length-1; index++) {
            if(reactive(list[index], list[index+1])){
                list[index] = "*";
                list[index + 1] = "*";
            }
        }
        const afterPass = list.filter(c => c !== "*");
        if(afterPass.length === list.length) break;
        list = afterPass;
    }
    return list;
}
const strip = (l:string[], stripChar:string)=>{
    const up = stripChar.toUpperCase();
    const down = stripChar.toLowerCase();
    return l.filter(c => c !== up && c !== down);
}

read("input/5.txt", "utf8").then((d) =>{
    const list = d.split("").filter(c => c !== "\n");
    const reacted = react(list);
    console.log(`After reaction, polymer is of length: ${reacted.length}`);

    var letters = "abcdefghijklmnopqrstuvwxyz".split("");
    const lettersWithScores = letters.map(l => {
        const stripped = strip(list, l);
        const reactedLength = react(stripped).length;
        return {letter: l, remain: reactedLength};
    });
    const best = lettersWithScores.sort(firstBy(s => s.remain, -1))[0];
    console.log(`Best removal is ${best.letter} with ${best.remain} remaining`);



});
