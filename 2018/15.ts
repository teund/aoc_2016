import { promisify } from "util";
import * as fs from "fs";
import { parseToObjects } from "./modules/lineParser";
import {Coord, Grid, GridPos} from "./modules/grid";
import { firstBy } from "thenby";
import { generateKeyPairSync } from "crypto";
const read = promisify(fs.readFile);

const sortReadingOrder = firstBy<Coord>(c => c.y).thenBy(c => c.x);

Array.prototype.flatMap = function(lambda) { 
    return this.map(lambda).reduce((arr: any[], v: any) => (arr.push(...v), arr), [] as any[]);
};

class MapBlock{
    type: string = "?";
}
const wall = new MapBlock();
wall.type = "#";
class Unit extends MapBlock{
    hitpoints: number = 200;
    attack: number = 3;
}
class Goblin extends Unit {
    constructor(){
        super();
        this.type = "G";
    }
}
class Elf extends Unit {
    constructor(){
        super();
        this.type = "E";
    }
}
const findShortestPathsBreadtFirst = (from: Coord, to: ((c: Coord) => boolean), access: ((c: Coord) => boolean) ) =>{
    let paths: Coord[][] = [[from]];
    const visitedEndpoints: {[k:string]: Coord} = {};
    while(paths.length > 0){
        const result = paths.filter(p => to(p[p.length-1]));
        if(result.length > 0){
            return result;
        }
        const expanded = paths.flatMap(p => {
            const neighbours = p[p.length-1].neighbours().filter(n => !(n.name() in visitedEndpoints))
                .filter(n => access(n));
            const expandedPaths = neighbours.map(n => {
                const np = p.slice(0);
                np.push(n);
                return np;
            });
            return expandedPaths;
        });
        // only take the best path per tip
        const bestPathsPerDestination = Object.values(expanded.reduce<{[k: string]: Coord[]}>((a,v) => {
            const target = v[v.length -1].name();
            if(target in a){
                const other = a[target];
                if(sortReadingOrder(v[1], other[1]) < 0){
                    a[target] = v;
                }
            }else{
                a[target] = v;
            }
            return a;
        }, {}));
        
        bestPathsPerDestination.forEach(p => {
            const tip = p[p.length-1];
            visitedEndpoints[tip.name()] = tip; 
        });
        paths = bestPathsPerDestination;
    }
    return [];
}
const getAdjacentFoes = (grid: Grid<MapBlock>, unit: GridPos<Unit>) => {
    return unit.pos.neighbours()
    .map(c => ({pos:c, val: grid.forCoord(c)}))
    .filter(gp => gp.val instanceof Unit && gp.val.type !== unit.val.type) as GridPos<Unit>[];
}

read("input/15.txt", "utf8").then((d) =>{
//      d = `#########
// #G......#
// #.E.#...#
// #..##..G#
// #...##..#
// #...#...#
// #.G...G.#
// #.....G.#
// #########`;
    const grid = new Grid<MapBlock>();
    grid.parseFromStringFunc(d, {'G': () => new Goblin(), 'E': () => new Elf(), '#': () => wall});
    let rounds = 0;
    combatRounds: while(true){
        // start round
        const units = Array.from(grid.positions()).filter(v => v.val instanceof Unit).sort((v1, v2) => sortReadingOrder(v1.pos, v2.pos)) as GridPos<Unit>[];
        for (const activeUnit of units) {
            if(activeUnit.val.hitpoints <= 0)continue;
            if(getAdjacentFoes(grid, activeUnit).length === 0){
                const foes = Array.from(grid.positions()).filter(v => v.val instanceof Unit).filter(other => other.val.type !== activeUnit.val.type);
                if(foes.length === 0)break combatRounds;
                const adjacent = foes.flatMap(f => f.pos.neighbours());
                const availableAdjacent = adjacent.filter(pos => grid.forCoord(pos) == null).map(c => c.name());
                if (adjacent.findIndex(a => activeUnit.pos.equals(a)) == -1){
                    const shortestPaths = findShortestPathsBreadtFirst(activeUnit.pos, 
                        (c) => availableAdjacent.indexOf(c.name()) > -1,
                        (c) => grid.forCoord(c) === null);
                    const preferredPaths = shortestPaths.sort(
                        firstBy((p1, p2) => sortReadingOrder(p1[p1.length-1], p2[p2.length-1]))
                        .thenBy((p1, p2) => sortReadingOrder(p1[1], p2[1])));
                    if(preferredPaths.length > 0){
                        // move
                        const toCell = preferredPaths[0][1];
                        grid.clear(activeUnit.pos);
                        grid.set(toCell, activeUnit.val);
                        activeUnit.pos = toCell;
                    }
                }
            }

            // attack
            const adjacentFoes: GridPos<Unit>[] = getAdjacentFoes(grid, activeUnit);
            if(adjacentFoes.length > 0){
                const target = adjacentFoes.sort(firstBy<GridPos<Unit>>(f => f.val.hitpoints).thenBy((f1, f2) => sortReadingOrder(f1.pos, f2.pos)))[0];
                target.val.hitpoints -= activeUnit.val.attack;
                if(target.val.hitpoints <= 0){
                    grid.clear(target.pos);
                }
            }
        }
        rounds++;

        const unitsAfterRound = Array.from(grid.positions()).filter(v => v.val instanceof Unit).sort((v1, v2) => sortReadingOrder(v1.pos, v2.pos)) as GridPos<Unit>[];

        console.log(`Round: ${rounds}`);
        console.log(grid.toString((c, v) => (v||{type:' '}).type));
        console.log(unitsAfterRound.map(u => `${u.val.type}: (${u.val.hitpoints})`).join(", "));

    }
    console.log(`Exited after ${rounds}`)
    const units = Array.from(grid.positions()).filter(v => v.val instanceof Unit).sort((v1, v2) => sortReadingOrder(v1.pos, v2.pos)) as GridPos<Unit>[];
    console.log(grid.toString((c, v) => (v||{type:' '}).type));
    console.log(units.map(u => `${u.val.type}: (${u.val.hitpoints})`).join(", "));

    const remainingHitpoints = units.reduce((a,v)=>a+v.val.hitpoints,0);
    console.log(`hitpoints x rounds = (${remainingHitpoints} x ${rounds}) = ${remainingHitpoints*rounds}`);

});    
