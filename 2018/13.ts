import { promisify } from "util";
import * as fs from "fs";
import { firstBy } from "thenby";
import * as zeros from "zeros";
import * as savePixels from "save-pixels";
const read = promisify(fs.readFile);

enum Dir { North=0, East, South, West }
enum Bend { SouthWest=1, SouthEast, Cross, Straight }
class Coord {
    x: number;
    y: number;

    constructor(x: number, y: number) { this.x = x; this.y = y; };
    name() {
        return `${this.x}x${this.y}`;
    }
    move(dir: Dir){
        const x = this.x + (dir === Dir.East?1:0) + (dir === Dir.West?-1:0);
        const y = this.y + (dir === Dir.South?1:0) + (dir === Dir.North?-1:0);
        return new Coord(x,y);
    }
}
class Track {
    constructor(public type: Bend, public coord: Coord) { }
}
class Cart {
    constructor(startPos: Coord, startDir: Dir) {
        this.position = startPos;
        this.dir = startDir;
        this.name = startPos.name();
    }
    public name: string;
    public position: Coord;
    public dir: Dir;
    private nextTurn: number = 0;
    private crossTurn(){
        this.dir = ((this.dir + [-1, 0, +1][this.nextTurn]) + 4) % 4;
        this.nextTurn = (this.nextTurn + 1) % 3;
    }
    public turn(b: Bend){
        const from: Dir = this.dir;
        switch (b) {
            case Bend.SouthEast:
                if(from == Dir.South)this.dir = Dir.East;
                if(from == Dir.West)this.dir = Dir.North;
                if(from == Dir.North)this.dir = Dir.West;
                if(from == Dir.East)this.dir = Dir.South;
                break;
            case Bend.SouthWest:
                if(from == Dir.South)this.dir = Dir.West;
                if(from == Dir.East)this.dir = Dir.North;
                if(from == Dir.North)this.dir = Dir.East;
                if(from == Dir.West)this.dir = Dir.South;
                break;
            case Bend.Cross:
                this.crossTurn();
                break;
            default:
                break;
        }
    }
}

Array.prototype.flatMap = function (lambda) {
    return Array.prototype.concat.apply([], this.map(lambda));
};

const bendForChar = (c: string) => {
    const map: { [k: string]: Bend } = {
        '|': Bend.Straight,
        '-': Bend.Straight,
        '<': Bend.Straight,
        '>': Bend.Straight,
        'v': Bend.Straight,
        '^': Bend.Straight,
        '\\': Bend.SouthEast,
        '/': Bend.SouthWest,
        '+': Bend.Cross
    };
    return map[c] || null;
}
const dirForChar = (c: string) => {
    const map: { [k: string]: Dir } = {
        '<': Dir.West,
        '>': Dir.East,
        'v': Dir.South,
        '^': Dir.North,
    };
    return map[c] || null;
}
const move = (cart: Cart, tracks: {[k:string]:Track}) => {
    const newPos = cart.position.move(cart.dir);
    cart.position = newPos;
    cart.turn(tracks[cart.position.name()].type);
}
const collision = (c: Cart, carts: Cart[]) => {
    return (carts.filter(
        c2 => c2 !== c && c.position.name() === c2.position.name()
        ).length > 0);
}
const dump = (tracks: {[k:string]:Track}, carts: Cart[]) => {
    var tracksValues =  Object.keys(tracks).map(k => tracks[k]);
    const maxX = tracksValues.reduce<number>((a,v) => Math.max(a, v.coord.x),0);
    const maxY = tracksValues.reduce<number>((a,v) => Math.max(a, v.coord.y),0);
    const places = Array.from(new Array(maxY+1),
        (val,index)=> Array.from(new Array(maxX+1),(val,index)=>' '));
    tracksValues.forEach(tr => {
        places[tr.coord.y][tr.coord.x] = '#';
    });
    carts.forEach(c => {
        places[c.position.y][c.position.x] = '-';
    });
    console.log(places.map(arr => arr.join('') + "\n").join(''));

}
read("input/13.txt", "utf8").then((d) => {
//     d=`/->-\\        
// |   |  /----\\
// | /-+--+-\\  |
// | | |  | v  |
// \\-+-/  \\-+--/
//   \\------/   `;
    const tracks = d.split('\n').flatMap((line, y) => {
        const spots = line.split('').map((c, x) => {
            const coord = new Coord(x, y);
            const tr: Bend = bendForChar(c);
            return new Track(tr, coord);
        }).filter(tr => tr.type !== null);
        return spots;
    });
    const carts = d.split('\n').flatMap((line, y) => {
        const spots = line.split('').map((c, x) => {
            const coord = new Coord(x, y);
            const dir: Dir = dirForChar(c);
            return new Cart(coord, dir);
        }).filter(tr => tr.dir !== null);
        return spots;
    });
    const tracksByPos = tracks.reduce<{[k:string]:Track}>((a,v)=> {a[v.coord.name()] = v; return a;}, {});
    for (let tick = 0; tick < 100000; tick++) {
        //dump(tracksByPos, carts);
        if(tick % 100 === 0)console.log(`Tick: ${tick}`);
        const cartsInOrder = carts.sort(firstBy(c => c.position.y).thenBy(c => c.position.x));
        for (const cart of cartsInOrder) {
            move(cart, tracksByPos);
            if(collision(cart, cartsInOrder)){
                console.log(cart.position.name());
                return;
            }
        }
    }
});
