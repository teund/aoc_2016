import { promisify } from "util";
import * as fs from "fs";
import { parseToObjects} from "./modules/lineParser";
import { firstBy } from "thenby";
import { Coord, Grid, Direction } from "./modules/grid";
const read = promisify(fs.readFile);

class Cube{
    constructor(public x: number, public y: number, public z: number, public size: number,
        ){}
    public botsInReach: NanoBot[] = [];
    *split(){
        const newSize = this.size/2;
        yield new Cube(this.x, this.y, this.z, newSize);
        yield new Cube(this.x + newSize, this.y, this.z, newSize);
        yield new Cube(this.x, this.y + newSize, this.z, newSize);
        yield new Cube(this.x, this.y, this.z + newSize, newSize);
        yield new Cube(this.x + newSize, this.y + newSize, this.z, newSize);
        yield new Cube(this.x + newSize, this.y, this.z + newSize, newSize);
        yield new Cube(this.x, this.y + newSize, this.z + newSize, newSize);
        yield new Cube(this.x + newSize, this.y + newSize, this.z + newSize, newSize);
    }
}
class NanoBot{
    constructor(public x: number, public y: number, public z: number, public r: number){}
    distanceMnhtn(other: NanoBot){
        return Math.abs(this.x-other.x) + Math.abs(this.y-other.y) + Math.abs(this.z-other.z);
    }
    intersectsCube(c: Cube){
        let dist = 0;
        dist += (this.x <= c.x) ? c.x - this.x : 0
        dist += (this.x >= c.x + c.size - 1) ? this.x - (c.x + c.size - 1) : 0
        dist += (this.y <= c.y) ? c.y - this.y : 0
        dist += (this.y >= c.y + c.size - 1) ? this.y - (c.y + c.size - 1) : 0
        dist += (this.z <= c.z) ? c.z - this.z : 0
        dist += (this.z >= c.z + c.size - 1) ? this.z - (c.z + c.size - 1) : 0
        return dist <= this.r;
    }
}
const popFromQueue = (q: Cube[]) => {
    // sort largest bot-set to end. Tie-break on closest to origin.
    q.sort(firstBy<Cube>(c => c.botsInReach.length)
        .thenBy(c => Math.min(
            Math.abs(c.x), Math.abs(c.x + c.size - 1), 
            Math.abs(c.y), Math.abs(c.y + c.size - 1), 
            Math.abs(c.z), Math.abs(c.z + c.size - 1) 
            ), {direction:-1}));
    return q.pop();
}
read("input/23.txt", "utf8").then((d) =>{
//     d = `pos=<10,12,12>, r=2
// pos=<12,14,12>, r=2
// pos=<16,12,12>, r=4
// pos=<14,14,14>, r=6
// pos=<50,50,50>, r=200
// pos=<10,10,10>, r=5`;
    const start = Date.now();
    const bots = parseToObjects(d, /pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)/, 
    s => (new NanoBot(Number(s[1]), Number(s[2]), Number(s[3]), Number(s[4]))));
    const maxXyz = bots.reduce((a,v) => Math.max(a, Math.abs(v.x), Math.abs(v.y), Math.abs(v.z)),0);
    const outerCubeRadius = Math.pow(2, Math.ceil(Math.log2(maxXyz)));

    // cube size 2 runs form -2 to 1
    const firstCube = new Cube(-outerCubeRadius, -outerCubeRadius, -outerCubeRadius, 2*outerCubeRadius);
    firstCube.botsInReach = bots;
    const queue = [firstCube];
    while(true){
        const next = popFromQueue(queue);
        if(next.size === 1){
            //this is the one
            console.log(`Bots in reach: ${next.botsInReach.length}`);
            console.log(`  coord: ${next.x},${next.y},${next.z}`);
            console.log(`  dist : ${Math.abs(next.x) + Math.abs(next.y) + Math.abs(next.z)}`);
            console.log(`  in   : ${Date.now() - start} ms`);
            break;
        }
        const children: Cube[] = Array.from(next.split());
        children.forEach(ch => {
            const inContact = next.botsInReach.filter(b => b.intersectsCube(ch));
            ch.botsInReach = inContact;
            queue.push(ch);
        });
    }

});    