import {promisify} from "util";
import * as fs from "fs";
const read = promisify(fs.readFile);
read("input/1.txt", "utf8").then((d) =>{
    const values = d.split("\n").map(Number);
    const result = values.reduce((a,v)=>a+v, 0);
    console.log(result);

});
