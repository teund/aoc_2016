import {promisify} from "util";
import * as fs from "fs";
import {firstBy} from "thenby";
import { parseToObjects} from "./modules/lineParser";
import {zeros} from "zeros";
import * as savePixels from "save-pixels";
const read = promisify(fs.readFile);


Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

interface Point {x: number; y: number; dx: number; dy: number;}
interface Boundary {left:number; top: number; right: number; bottom: number;}

const displayPoints = (points: Point[], time: number) => {
    const boundaries = points.reduce((a,v) => {
        return {
            left: Math.min(v.x, a.left),
            top: Math.min(v.y, a.top),
            right: Math.max(v.x, a.right),
            bottom: Math.max(v.y, a.bottom)
        };
    }, {left:0, top: 0, right: 0, bottom: 0});
    const data: any = zeros([Math.min(1000, boundaries.right - boundaries.left)+5, Math.min(1000, boundaries.bottom - boundaries.top)+5]);
    points.forEach(p => {
        data.set(p.x, p.y, 255);
    });
    const stream = fs.createWriteStream(`output/10.${time}.png`);
    savePixels(data, "png").pipe(stream);
};

const nextConstellation = (p: Point[]) => {
    return p.map(point => ({x: point.x + point.dx, y: point.y + point.dy, dx: point.dx, dy: point.dy}));
};
const countNeighbours = (p:Point, all:Point[]) => {
    return all.filter(p2 => areNeighbours(p, p2)).length;
};
const areNeighbours = (p1: Point, p2: Point) => {
    return Math.abs(p1.x-p2.x) === 1 && Math.abs(p1.y-p2.y) === 1;
};
const pointsCloseTogether = (all: Point[]) => {
    const samplePoints = all;
    const neighbourCount = samplePoints.map(p => {
        return countNeighbours(p, all);
    });
    return neighbourCount.filter(n => n>0).length > all.length/4;
};

read("input/10.txt", "utf8").then((d) =>{
//     d = `position=< 9,  1> velocity=< 0,  2>
// position=< 7,  0> velocity=<-1,  0>
// position=< 3, -2> velocity=<-1,  1>
// position=< 6, 10> velocity=<-2, -1>
// position=< 2, -4> velocity=< 2,  2>
// position=<-6, 10> velocity=< 2, -2>
// position=< 1,  8> velocity=< 1, -1>
// position=< 1,  7> velocity=< 1,  0>
// position=<-3, 11> velocity=< 1, -2>
// position=< 7,  6> velocity=<-1, -1>
// position=<-2,  3> velocity=< 1,  0>
// position=<-4,  3> velocity=< 2,  0>
// position=<10, -3> velocity=<-1,  1>
// position=< 5, 11> velocity=< 1, -2>
// position=< 4,  7> velocity=< 0, -1>
// position=< 8, -2> velocity=< 0,  1>
// position=<15,  0> velocity=<-2,  0>
// position=< 1,  6> velocity=< 1,  0>
// position=< 8,  9> velocity=< 0, -1>
// position=< 3,  3> velocity=<-1,  1>
// position=< 0,  5> velocity=< 0, -1>
// position=<-2,  2> velocity=< 2,  0>
// position=< 5, -2> velocity=< 1,  2>
// position=< 1,  4> velocity=< 2,  1>
// position=<-2,  7> velocity=< 2, -2>
// position=< 3,  6> velocity=<-1, -1>
// position=< 5,  0> velocity=< 1,  0>
// position=<-6,  0> velocity=< 2,  0>
// position=< 5,  9> velocity=< 1, -2>
// position=<14,  7> velocity=<-2,  0>
// position=<-3,  6> velocity=< 2, -1>`;
    let points: Point[] = parseToObjects<Point>(d, /^position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>/, (s) => 
        ({x: Number(s[1]), y: Number(s[2]), dx: Number(s[3]), dy: Number(s[4])}));
    
    
        let i = 0;
        while(true){
            points = nextConstellation(points);
            i++;
            if(pointsCloseTogether(points)){
                displayPoints(points, i);
            }
            if(i > 20000) {break; }
        }

});
