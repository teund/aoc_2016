import { Coord, Grid, Direction } from "./modules/grid";

const depth = 11541;
const target = new Coord(14,778);
// const depth = 510;
// const target = new Coord(10,10);

const grid = new Grid<string>();
const start = new Coord(0,0);
grid.add(start, "X");
grid.add(target, "T");

const geoIndex = (c: Coord): number =>{
    if(c.name() == "0x0")return 0;
    if(c.name() == target.name())return 0;
    if(c.y === 0) return c.x * 16807;
    if(c.x === 0) return c.y * 48271;
    return erosionLevel(c.above) * erosionLevel(c.left);

}
const elCache: {[k:string]: number} = {};
const erosionLevel = (c: Coord): number =>{
    const cname = c.name();
    if(cname in elCache) return elCache[cname];
    const geoIx = geoIndex(c);
    const el = (geoIx + depth) % 20183;
    elCache[cname] = el;
    return el;
}
console.log(grid.toString((c, v) => {
    if(v) return v;
    const lvl = erosionLevel(c);
    return [".", "=", "|"][lvl % 3];
}));
const bound = grid.boundaries();
var risk = 0;
for (const pos of Coord.forRectangle(bound[0].x, bound[1].x, bound[0].x, bound[1].y)) {
    const lvl = erosionLevel(pos);
    risk += lvl % 3;
}
console.log(risk); 