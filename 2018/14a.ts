import { promisify } from "util";
import * as fs from "fs";
import {StringAppender} from "./modules/appender";
const read = promisify(fs.readFile);

const input = 430971;
//const input = '92510';

class State {
    public recipes: StringAppender = new StringAppender();
    elf1Current: number;
    elf2Current: number;
    work(){
        const sum = Number(this.recipes.get(this.elf1Current)) + Number(this.recipes.get(this.elf2Current));
        this.recipes.append(sum.toString());

        this.elf1Current += Number(this.recipes.get(this.elf1Current)) + 1;
        this.elf2Current += Number(this.recipes.get(this.elf2Current)) + 1;
        this.elf1Current = this.elf1Current % this.recipes.length;
        this.elf2Current = this.elf2Current % this.recipes.length;
    }
}

const state = new State();
state.recipes.append("37");
state.elf1Current = 0;
state.elf2Current = 1;
const stringToLookFor = input.toString();
let it = 0;
while(true){
    it++;
    state.work();

    const tail = state.recipes.tail(10);
    if(tail.indexOf(stringToLookFor) > -1){
        break;
    }
    if(it % 10000 === 0){
        console.log(`Iteration ${it}: ${state.recipes.length} recipes.`);
    }

}
console.log(`looking for ${stringToLookFor}. End was ${state.recipes.tail(10)}`);
console.log(state.recipes.length - stringToLookFor.length);
