var fs = require("fs");
fs.readFile("input/input25.txt", "utf8", (e, data) => {
    var commands = data.trim().split("\n");
    var ops = commands.map(oppify);

    var a = 0;
    while(true){
        var ctx = {
            a:a,
            b:0,
            c:0,
            d:0,
            o:[],
            success:false,
            _p:0
        }
        var i = 0;
        while(ctx._p >= 0 && ctx._p < ops.length){
            var op = ops[ctx._p];
            //if(i%10000000 === 0)console.log(op.cmd, ctx);
            op.exec(ctx, ops);
            i++;
        }
        console.log(ctx);
        if(ctx.success){
            break;
        }
        a++;
    }

    console.log(a, ctx.o);
    
});

function getValue(v){
    var res = function(ctx){
        if(isNaN(v)){
            return ctx[v];
        }
        return Number(v);
    }
    res.set = function(ctx, val){ctx[v] = val;}
    return res;
}

function oppify(line){
    var parts = line.split(' ');
    var cmd = parts[0];
    var args = parts.slice(1, parts.length).map(getValue);
    var op = {
        cmd:line,
        args:args,
        next:function(ctx){ctx._p++;},
        exec:function(ctx){this.next(ctx);}
    }
    var execs = {
        "cpy":function cpy(ctx){
            this.args[1].set(ctx, args[0](ctx));
            this.next(ctx);
        },
        "inc": function inc(ctx){
            this.args[0].set(ctx, args[0](ctx) + 1);
            this.next(ctx);
        },
        "dec": function dec(ctx){
            this.args[0].set(ctx, args[0](ctx) - 1);
            this.next(ctx);
        },
        "jnz":function jnz(ctx){
            var pred = this.args[0](ctx);
            if(pred === 0){
                this.next(ctx);
            }else{
                ctx._p += this.args[1](ctx);
            }
        },
        "out": function out(ctx){
            var output = this.args[0](ctx);
            ctx.o.push(output);
            if((ctx.o.length % 2 == 1) ? output == 0 : output == 1){
                if(ctx.o.length > 20){
                    ctx.success = true;
                    ctx._p = 1000;
                }
            }else{
                ctx._p = 1000;
            }
            this.next(ctx);
        }
    }
    op.exec = execs[cmd] || function(ctx){op.next(ctx);};
    return op;
}