import fs = require("fs");

let reAbba = /(\w)(\w)\2\1/g;
function abbaInString(s:string){
    let matches = s.match(reAbba);
    if(matches === null){return false;}
    return matches.filter(m => m[0] !== m[1]).length > 0;
}


fs.readFile("input/input7.txt", "utf8", (e, data) => {
    // let addresses = [
    //     "abba[mnop]qrst",
    //     "abcd[bddb]xyyx",
    //     "aaaa[qwer]tyui",
    //     "ioxxoj[asdfgh]zxcvb"
    // ];
    let addresses = data.split("\n");
    let reInBrackets = /\[\w+\]/g;

    let tls = addresses.filter(s => {
        if(!abbaInString(s)){return false;}

        let bracketed = s.match(reInBrackets);
        return bracketed.filter(abbaInString).length === 0;
    });

    console.log(tls);
    console.log(tls.length, "TLS addresses");
});
