var fs = require("fs");
var SIZE ={x:50, y:6};
fs.readFile("input/input9.txt", "utf8", (e, data) => {
    //data = "(3x2)(2x3)";
    data = data.trim();
    var reBlock = /\((\d+)x(\d+)\)/;
    var result = "";
    while(true){
        var match = reBlock.exec(data);
        if(match == null){
            result += data;
            break;
        }else{
            var l = Number(match[1]);
            var times = Number(match[2]);
            var repeated = data.substring(match.index + match[0].length , match.index + match[0].length + l).repeat(times);
            result += data.substring(0, match.index) + repeated;
            data = data.substring(match.index + match[0].length + l);
        }
    }
    console.log(result.length);
    console.log("last char", result[result.length - 1]);
});

