var input = [
{num:1, pos:7,  start: 0},
{num:2, pos:13, start: 0},
{num:3, pos:3,  start: 2},
{num:4, pos:5,  start: 2},
{num:5, pos:17, start: 0},
{num:6, pos:19, start: 7}
// uncomment for 15a
//, {num:7, pos:11, start:0}
];

var second = 0;
while(true){
    var firstFail = input.find(function(disc){
        return (disc.start + second + disc.num) % disc.pos !== 0;
    });
    if(!firstFail){
        console.log("found match for all! after", second);
        break;
    }
    if(firstFail.num > 3)console.info("Second: ", second, "failed on disc ", firstFail.num);
    second++;
}