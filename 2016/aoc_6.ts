import fs = require("fs");
fs.readFile("input/input6.txt", "utf8", (e, data) => {
    let cols = [0,1,2,3,4,5,6,7];
    let words = data.split("\n").filter(w => w.length === 8);
    let frequencies = cols.map(i => {
        return words.map(w => w[i])
            .reduce<{[key:string]:number}>((a, c) => {
                a[c] = (a[c]||0) + 1;
                return a;
            }, {});
    });
    let letters = frequencies.map(f => {
        return Object.keys(f).reduce<[string, number]>(
            (a, c)=> f[c] > a[1]?[c, f[c]]:a,
            ['', 0]
        )[0];

    });
    console.log(letters.join(''));
});
