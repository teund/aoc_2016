var input = 3018458;
var state = new Array(input-1);
for(var i = 1; i<= input; i++){
    state[i-1] = i;
    if(i%100000 === 0)console.log("filling,", i);
}
var index = 0;
var nextPos = i => {
    for(var ix = i+1; ix < state.length; ix++){
        if(state[ix] !== null)return ix;
    }
    for(var ix = 0; ix < i; ix++){
        if(state[ix] !== null)return ix;
    }
    return undefined;
}
var todo = input;
while(todo > 1){
    // remove next from index
    state[nextPos(index)] = null;
    todo--;
    if(todo %100000 === 0)console.log("to do", todo);
    index = nextPos(index);
}

console.log(nextPos(-1)+1);

