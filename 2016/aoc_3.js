fs = require('fs')
fs.readFile('input/input3.txt', 'utf8', function (err,data) {
	var lines = data.split("\n");
	var triangles = lines
		.map(function(line){
			var sides = line.split(/\s+/)
				.filter(function(s){return s.length > 0;})
				.map(function(w){return parseInt(w);})
				.sort(function(n1,n2){return n1>n2?-1:1});
			return sides;
		})
		.filter(function(l){return l.length === 3;});
	var possible = triangles.filter(function(t){return t[0] < t[1] + t[2];})

	console.log(triangles.length, "triangles");
	console.log(possible.length, "possible");
	console.log(triangles.length-possible.length, "impossible");

});

