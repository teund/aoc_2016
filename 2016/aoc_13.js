var fs = require("fs");
var fb = require("thenby");

var target = {x:31, y:39};
var start =  {x:1, y:1};

function openSpace(x, y){
    var magicNumber = 1358;
    var fullNum = x*x + 3*x + 2*x*y + y + y*y + magicNumber;
    return fullNum.toString(2).replace(/0/g, "").length % 2 === 0
}
function token(coord){
    return coord.x + "," + coord.y;
}
function findMostPromisingEndpoint(reached){
    var keysToEvaluate = Object.keys(reached).filter(function(key){return !reached[key].evaluated;}); 
    var orderedKeys = keysToEvaluate.sort(fb(function(key){
            var endpoint = reached[key];
            var pos = endpoint.path[endpoint.path.length - 1];
            return (endpoint.path.length - 1) + Math.abs(pos.x - target.x) +  Math.abs(pos.y - target.y); 
        }) );
    return reached[orderedKeys[0]];
}
function addNextStepsFrom(from, to){
    [[+1, 0], [-1, 0], [0, +1], [0, -1]].forEach(function(move){
        var oldpos = from.path[from.path.length - 1];
        var newPos = {x: oldpos.x + move[0], y: oldpos.y + move[1]};
        if(newPos.x < 0 || newPos.y < 0)return;
        if(token(newPos) in to)return;
        if(!openSpace(newPos.x, newPos.y))return;
        var newPath = from.path.slice(0);
        newPath.push(newPos);
        to[token(newPos)] = {path:newPath, evaluated:false};
    });
    from.evaluated = true;
}

// A*
var reachedEndpoints = {};
reachedEndpoints[token(start)] = {path:[start], evaluated:false};
while(! (token(target) in reachedEndpoints)){
    var head = findMostPromisingEndpoint(reachedEndpoints);
    console.log(head.path);
    addNextStepsFrom(head, reachedEndpoints);
}

console.log("Reached points", Object.keys(reachedEndpoints).length);
console.log("winning", reachedEndpoints[token(target)]);
console.log("winning", reachedEndpoints[token(target)].path.length - 1);
