var fs = require("fs");
var tb = require("thenby");
fs.readFile("input/input21.txt", "utf8", (e, data) => {
    var mutations = data.trim().split('\n').map(l => l.trim()).map(l => {return {full: l, parts: l.split(' ')};})
        .map(cmd => { 
            if(cmd.full.indexOf('swap position') === 0){
                return function(s){
                    var s2 = s.slice(0);
                    s2[cmd.parts[2]] = s[cmd.parts[5]];
                    s2[cmd.parts[5]] = s[cmd.parts[2]];
                    return s2;
                }
            }
            if(cmd.full.indexOf('swap letter') === 0){
                return function(s){
                    return s.map(c => c === cmd.parts[2] ? cmd.parts[5] : c === cmd.parts[5] ? cmd.parts[2] : c);
                }
            }
            function _rot(s, steps){ return s.slice(steps).concat(s.slice(0, steps));}

            if(cmd.full.indexOf('rotate left') === 0){
                return function(s){
                    var steps = Number(cmd.parts[2]);
                    return _rot(s, steps);
                }
            }
            if(cmd.full.indexOf('rotate right') === 0){
                return function(s){
                    var steps = s.length - Number(cmd.parts[2]);
                    return _rot(s, steps);
                }
            }
            if(cmd.full.indexOf('rotate based') === 0){
                return function(s){
                    var stepsRight = 1 + s.indexOf(cmd.parts[6]);
                    if(s.indexOf(cmd.parts[6]) >= 4) stepsRight++;
                    return _rot(s, s.length - stepsRight);
                }
            }
            if(cmd.full.indexOf('reverse positions') === 0){
                return function(s){
                    var s2 = s.slice(0);
                    for(var i = 0; i <= cmd.parts[4]-cmd.parts[2];i++){
                        s2[Number(cmd.parts[2]) + i] = s[cmd.parts[4]-i];
                    }
                    return s2;
                }
            }

            if(cmd.full.indexOf('move position') === 0){
                return function(s){
                    var c = s.splice(cmd.parts[2], 1);
                    return s.slice(0, cmd.parts[5]).concat(c).concat(s.slice(cmd.parts[5]));
                }
            }


            return function(s){
                s.push('#');return s;
            }
        });
    var input = "cbadeghf".split('');
    var result= mutations.reduce((a, func) => {
      var r = func(a);
      console.log(r.join(''));
      return r;
    }, input);
    
    
    console.log(result.join(''));
});
