var fs = require("fs");
var tb = require("thenby");
var combi = require("js-combinatorics");

fs.readFile("input/input24.txt", "utf8", (e, data) => {
    var grid = data.trim().split("\n").map(l => l.trim());
    var spots = Array.from(getSpots(grid)).sort(tb('c'));
    var distances = getDistancesBetween(spots, grid);

    var c = combi.permutation(spots.slice(1));
    var minDist = Infinity;
    var order;
    while( order = c.next()){
        order.unshift(spots[0]);
        order.push(spots[0]);
        var dist = calcLength(order, distances);
        if(dist < minDist)minDist = dist;
    }

    console.log(minDist);
    
});

function *getSpots(grid){
    for(var y = 0; y< grid.length; y++){
        for(var x = 0; x< grid[y].length; x++){
            if(isSpot(grid[y][x])){
                yield {c: grid[y][x], x: x, y: y};
            }
        }
    }
}
function isSpot(char){
    return char >= '0' && char <= '9';
}
function token(spot){
    return spot.x + "_" + spot.y;
}
function getNeighbours(expand, grid){
    return [[1,0], [-1,0], [0, 1], [0, -1]].map(d => {
        return {x: expand.x + d[0], y: expand.y + d[1], steps: expand.steps + 1};
        })
        .map(n => {n.c = grid[n.y][n.x]; return n;})
        .filter(pos => pos.c !== '#');
}
function *distFromSpot(start, grid){
    start.steps = 0;
    var seen = {};
    var toExpand = [start];
    seen[token(start)] = true;
    while(toExpand.length > 0){
        var expand = toExpand.shift();
        for(var nextSpot of getNeighbours(expand, grid)){
            if(! (token(nextSpot) in seen)){
                if(isSpot(grid[nextSpot.y][nextSpot.x])){
                    //console.log("from", start, "to", nextSpot, grid[nextSpot.y][nextSpot.x]);
                    yield {from: start, to: nextSpot};
                }
                seen[token(nextSpot)] = true;
                toExpand.push(nextSpot);
            }
        }
    }
}
function getDistancesBetween(spots, grid){
    var allDistances = spots.reduce((a, s) => {
        var newDistances = Array.from(distFromSpot(s, grid));
        return a.concat(newDistances);
    }, []);
    return allDistances.map(d => [d.from.c + "->" + d.to.c, d.to.steps])
      .reduce((a,v)=> {a[v[0]] = v[1]; return a;}, {});
}
function calcLength(spots, distances){
    return spots.reduce((a, v, i, arr)=>{
        if(i >= arr.length-1)return a;
        var from = arr[i];
        var to = arr[i+1]
        return a + distances[from.c + "->" + to.c];
    }, 0);
}