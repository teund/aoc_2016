var size = 35651584;//272;
var input = "01111010110010011";

var reversedInput = input.split("").reverse().map(c => c==="1"?"0":"1").join('');

var powersOf2 = Array.from({length:62}, (v,k)=> Math.pow(2,k));
function nextPow2(n){return powersOf2.find(v => v > n);}

function foldUp(index){
    var nextPow = nextPow2(index);
    if(nextPow === 2*index) return true;
    return !foldUp(nextPow - index);
}

function *getDiskContent(){
    var pos = 0;
    var block = 0;

    while(true){
        var str = block%2==0 ? input : reversedInput;
        for(var pos = 0; pos<input.length;pos++){
            yield str[pos];
        }
        yield foldUp(block+1) ? '0' : '1';
        block++;
    }
}
function *limit(gen, size){
    var i = 0;
    for(var o of gen){
        yield o;
        if(i++ >= size)break;
    }
}
function *checksumStep (gen){
    var chunk = [];
    for(var c of gen){
        chunk.push(c);
        if(chunk.length === 2){
            yield (chunk[0] === chunk[1]) ? "1" : "0";
            chunk = [];
        }
    }
}

function constructMultistepChecksummer(size){

    if(size/2 % 2 === 1){
        return checksumStep;
    }
    var inner = constructMultistepChecksummer(size/2);
    return function(gen){
        return checksumStep(inner(gen));
    }
    
}

var gen = limit(getDiskContent(), size);
var compact = constructMultistepChecksummer(size);
var checksum = compact(gen);

console.log(Array.from(checksum).join(''));
