var fs = require("fs");
var SIZE ={x:50, y:6};
fs.readFile("input/input9.txt", "utf8", (e, data) => {
    //data = "(3x2)(2x3)";
    data = data.trim();
    var reBlock = /\((\d+)x(\d+)\)/;
    function getLength(d){
        var result = 0;
        while(true){
            var match = reBlock.exec(d);
            if(match == null){
                result += d.length;
                break;
            }else{
                var l = Number(match[1]);
                var times = Number(match[2]);
                var startOfRepetition = match.index + match[0].length;
                var expandedLength = getLength(d.substring(startOfRepetition, startOfRepetition + l));
                result += match.index + expandedLength*times;
                d = d.substring(startOfRepetition + l);
            }

        }
        return result;
    }
    console.log(getLength(data));
});

