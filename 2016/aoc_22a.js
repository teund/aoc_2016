var fs = require("fs");
//var combinatorics = require('js-combinatorics');
var fb = require("thenby");

fs.readFile("input/input22.txt", "utf8", (e, data) => {
    
    var lines = data.trim().split('\n').map(s => s.trim());
    var reParts = /\/dev\/grid\/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s/;
    var nodes = lines.map(l =>{
        var match = reParts.exec(l);
        if(match){
            return {
                x: parseInt(match[1]),
                y: parseInt(match[2]),
                size: parseInt(match[3]),
                used: parseInt(match[4]),
                fixed: parseInt(match[4]) >= 100
            }
        }
        return null;
    }).filter(n => n !== null);
    var nodesByPos = nodes.reduce((a, n) => {
        a[n.x + "_" + n.y] = n;
        return a;
    },{});
    var emptyNode = nodes.find(n => n.used === 0);
    var targetNode = nodes.filter(n => n.y === 0).sort(fb('x', -1))[0];
    var startState = {target:{x:targetNode.x, y: targetNode.y}, empty:{x:emptyNode.x, y:emptyNode.y}, steps:0};
    var toExpand = [startState];
    var seen = {max:-1};
    function token(state){return state.empty.x + "_" + state.empty.y + "_" + state.target.x + "_" + state.target.y;}
    function getMoves(state){
        if(state.steps > seen.max){
            console.log(state);
            seen.max = state.steps;
        }
        var moving = [[1,0], [-1,0], [0,-1], [0, 1]].map(m => { 
            return {
                x: state.empty.x + m[0], 
                y: state.empty.y + m[1]
            };})
            .filter(p => (p.x + "_" + p.y) in nodesByPos)
            .filter(p => !nodesByPos[p.x + "_" + p.y].fixed);
        var newStates = moving.map(m =>{
            return {
                steps: state.steps+1,
                empty:{x:m.x, y:m.y}, 
                target: m.x === state.target.x && m.y === state.target.y ? state.empty : state.target
            };
        });
        var unseenStates = newStates.filter(s => !(token(s) in seen));
        return unseenStates;
    }


    var found = null;
    while(found === null && toExpand.length > 0){
        var expand = toExpand.shift();
        var moves = getMoves(expand, seen);
        moves.forEach(s => {
            if(s.target.x === 0 && s.target.y === 0)found = s;
            toExpand.push(s);
            seen[token(s)] = s;
        });


    }
    console.log(found);
});
