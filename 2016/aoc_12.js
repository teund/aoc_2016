var fs = require("fs");
var SIZE ={x:50, y:6};
fs.readFile("input/input12.txt", "utf8", (e, data) => {
    var commands = data.trim().split("\n");
    var ops = commands.map(oppify);

    var ctx = {
        a:0,
        b:0,
        c:0,
        d:0,
        _p:0
    }
    var i = 0;
    while(ctx._p >= 0 && ctx._p < ops.length){
        var op = ops[ctx._p];
        if(i%100 === 0)console.log(op.cmd, ctx);
        op.exec(ctx);
        i++;
    }

    console.log(ctx);
    
});

function getValue(v){
    var res = function(ctx){
        if(isNaN(v)){
            return ctx[v];
        }
        return Number(v);
    }
    res.set = function(ctx, val){ctx[v] = val;}
    return res;
}

function oppify(line){
    var parts = line.split(' ');
    var cmd = parts[0];
    var args = parts.slice(1, parts.length).map(getValue);
    var op = {
        cmd:line,
        args:args,
        next:function(ctx){ctx._p++;},
        exec:function(ctx){this.next(ctx);}
    }

    if(cmd === "cpy"){
        op.exec = function(ctx){
            op.args[1].set(ctx, args[0](ctx));
            op.next(ctx);
        }
    }
    if(cmd === "inc"){
        op.exec = function(ctx){
            op.args[0].set(ctx, args[0](ctx) + 1);
            op.next(ctx);
        }
    }
    if(cmd === "dec"){
        op.exec = function(ctx){
            op.args[0].set(ctx, args[0](ctx) - 1);
            op.next(ctx);
        }
    }
    if(cmd === "jnz"){
        op.exec = function(ctx){
            var pred = op.args[0](ctx);
            if(pred === 0){
                op.next(ctx);
            }else{
                ctx._p += args[1](ctx);
            }
        }
    }
    return op;
}