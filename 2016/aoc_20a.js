var fs = require("fs");
var fb = require("thenby");
fs.readFile("input/input20.txt", "utf8", (e, data) => {
    var blocks = data.trim().split('\n').map(l => l.trim()).map(l => l.split('-'))
        .map(parts => { return {from: parseInt(parts[0]), to: parseInt(parts[1])}});
    var free = blocks.sort(fb('from')).reduce((acc, v) => {
        if(v.from > acc.highestSeen + 1){
            acc.blocks.push([acc.highestSeen + 1, v.from - 1]);
        }
        if(v.to > acc.highestSeen)acc.highestSeen = v.to;
        return acc;
    }, {blocks:[], highestSeen:0});
    
    console.log(free.blocks.reduce((acc, v)=> {
            return acc + 1 + v[1]-v[0]
        }, 0));
});
