var fs = require("fs");
var SIZE ={x:50, y:6};
fs.readFile("input/input8.txt", "utf8", (e, data) => {
    var commands = data.split("\n");
    var ops = commands.map(oppify).reverse();
    ops.push({name:"blank", exec:function(p){p.todo=null; p.on=false; return p;}})

    var pixels = [];
    for (var x=0;x < SIZE.x; x++){
       for(var y = 0; y<SIZE.y; y++){
           pixels.push({x:x, y:y});
       } 
    }
    var on = pixels.map(function(p){
        return findOnOff({x:p.x, y:p.y, on:null, todo:0}, ops);
    });
    console.log(on.filter(Boolean).length);
    
});

function findOnOff(p, ops){
    while(p.on === null && p.todo !== null){
        var op = ops[p.todo]
        op.exec(p);
        if(p.todo != null)p.todo++;
    }
    return p.on;
}
function oppify(comm){
    var parts = comm.split(' ');

    if(parts[0] === "rect"){
        var x = parseInt(parts[1].substring(0, parts[1].indexOf('x')));
        var y = parseInt(parts[1].substring(parts[1].indexOf('x')+1));
        return {
            name: comm,
            exec: function(p){
                if(p.x < x && p.y < y){
                    p.on = true;
                    p.todo = null;
                }
            }
        };
    }
    if(parts[0] === "rotate" && parts[1] == "row"){
        var y = parseInt(parts[2].substring(2));
        var step = parseInt(parts[4]);
        return {
            name: comm,
            exec: function(p){
                if(p.y === y){
                    p.x -= step;
                    if(p.x < 0)p.x += SIZE.x;
                }
            }
        };
    }
    if(parts[0] === "rotate" && parts[1] == "column"){
        var x = parseInt(parts[2].substring(2));
        var step = parseInt(parts[4]);
        return {
            name: comm,
            exec: function(p){
                if(p.x === x){
                    p.y -= step;
                    if(p.y < 0)p.y += SIZE.y;
                }
            }
        };
    }

    return {name:"nop", exec:function(p){}}
}