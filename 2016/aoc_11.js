var fs = require("fs");
var combinatorics = require('js-combinatorics');

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + String(character) + this.substr(index+String(character).length);
}
var isotopes = [];
var indexForIsotope = {};

fs.readFile("input/input11.txt", "utf8", (e, data) => {
    var lines = data.trim().split('\n');
    lines.forEach(function(l){
        var re = /\b(\S+) generator/g;
        var match;
        while(match = re.exec(l)){
            isotopes.push(match[1]);
        }
    });
    isotopes.forEach(function(e, i){indexForIsotope[e] = i;});

    var state = "1" + "  ".repeat(isotopes.length);
    var desiredState = "4".repeat(isotopes.length * 2 + 1);
    lines.forEach(function(l, i){
        var re = /\b(\S+) generator/g;
        var match;
        while(match = re.exec(l)){
            state = state.replaceAt(indexForIsotope[match[1]] *2 + 1, String((i+1)));
        }
    });
    lines.forEach(function(l, i){
        var re = /\b(\S+)-compatible microchip/g;
        var match;
        while(match = re.exec(l)){
            state = state.replaceAt(indexForIsotope[match[1]] *2 + 2, String((i+1)));
        }
    });
    var moves = 0;
    var seenStates = {}; seenStates[state] = true;
    var statesToEvaluate = {}; statesToEvaluate[state] = true;
    var evalCounter = 1;
    while(true){
        Object.keys(statesToEvaluate).forEach(function(toEvaluate){
            var nextPossible = getNextPossible(toEvaluate);
            nextPossible.forEach(function(s){
                if(!(s in seenStates))
                {
                    seenStates[s] = true; 
                    if(!fry(s)) statesToEvaluate[s] = true;
                    evalCounter++;
                }
            });
            delete statesToEvaluate[toEvaluate];
            evalCounter--;
        });
        moves++;
        console.log("evaluate move", moves, "To check in next move: ", evalCounter );
        if(desiredState in seenStates)break;
        if(evalCounter === 0)break;
    }

    console.log(moves);

});
function getNextPossible(from){
    var lift = from[0];
    var elemsAtLiftLevel = from.substring(1).split('')
        .map(function(c,i){return i+1;})
        .filter(function(i){return from[i] == lift});
    var allCombinations = [];
    if(elemsAtLiftLevel.length > 0){
        allCombinations = allCombinations
            .concat(combinatorics.combination(elemsAtLiftLevel, 1).toArray());
    }
    if(elemsAtLiftLevel.length > 1){
        allCombinations = allCombinations
            .concat(combinatorics.combination(elemsAtLiftLevel, 2).toArray());
    }
    var newStatesUp = getNewStates(from, allCombinations, +1);
    var newStatesDown = getNewStates(from, allCombinations, -1);
    return newStatesUp.concat(newStatesDown);
}

function getNewStates(from, allCombinations, delta){
    var newFloor = Number(from[0]) + delta;
    if(newFloor < 1 || newFloor > 4)return [];
    from = from.replaceAt(0, newFloor);
    return allCombinations.map(function(combi){
        return combi.reduce(function(a, v){
            return a.replaceAt(v, newFloor);
        }, from);
    });
}
function fry(s){
    for(var iMc = 2; iMc < s.length; iMc += 2){
        if(s[iMc] !== s[iMc-1]){
            // vulnerable
            for(var iG = 1; iG < s.length; iG += 2){
                if(s[iMc] === s[iG]){
                    return true;
                }
            }
        }
    }
    return false;
}