var fs = require("fs");
fs.readFile("input/input23.txt", "utf8", (e, data) => {
    var commands = data.trim().split("\n");
    var ops = commands.map(oppify);

    var ctx = {
        a:7,
        b:0,
        c:0,
        d:0,
        _p:0
    }
    var i = 0;
    while(ctx._p >= 0 && ctx._p < ops.length){
        var op = ops[ctx._p];
        op.exec(ctx, ops);
        i++;
    }

    console.log(ctx);
    
});

function getValue(v){
    var res = function(ctx){
        if(isNaN(v)){
            return ctx[v];
        }
        return Number(v);
    }
    res.set = function(ctx, val){ctx[v] = val;}
    return res;
}

function oppify(line){
    var parts = line.split(' ');
    var cmd = parts[0];
    var args = parts.slice(1, parts.length).map(getValue);
    var op = {
        cmd:line,
        args:args,
        next:function(ctx){ctx._p++;},
        exec:function(ctx){this.next(ctx);}
    }
    var execs = {
        "cpy":function cpy(ctx){
            this.args[1].set(ctx, this.args[0](ctx));
            this.next(ctx);
        },
        "inc": function inc(ctx){
            this.args[0].set(ctx, this.args[0](ctx) + 1);
            this.next(ctx);
        },
        "dec": function dec(ctx){
            this.args[0].set(ctx, this.args[0](ctx) - 1);
            this.next(ctx);
        },
        "jnz":function jnz(ctx){
            var pred = this.args[0](ctx);
            if(pred === 0){
                this.next(ctx);
            }else{
                ctx._p += this.args[1](ctx);
            }
        },
        "tgl":function tgl(ctx, ops){
            var subject = ops[ctx._p + this.args[0](ctx)];
            if(subject && subject.args.length === 1){
                subject.exec = subject.exec.name === "inc" ? execs.dec : execs.inc;
            }
            if(subject && subject.args.length === 2){
                subject.exec = subject.exec.name === "jnz" ? execs.cpy : execs.jnz;
            }
            this.next(ctx);
        }
    }
    op.exec = execs[cmd] || function(ctx){op.next(ctx);};
    return op;
}