import fs = require("fs");

var anyChange : boolean = false;
var bots : Bot[] = [];
var outputs : number[][] = [];

class Bot{
    private chips: number[] = [];
    myNumber : number;
    upperTo: number;
    upperType:string;
    lowerTo: number;
    lowerType:string;
    do(){
        if(this.chips.length === 2){
            let sorted = this.chips.sort((n1,n2) => n1-n2);
            console.log("Bot", this.myNumber, "comparing", sorted);
            if(this.lowerType === "bot"){
                bots[this.lowerTo].accept(sorted[0]);
            }else{
                if(!outputs[this.lowerTo]) {outputs[this.lowerTo]=[];}
                outputs[this.lowerTo].push(sorted[0]);
            }
            if(this.upperType === "bot"){
            bots[this.upperTo].accept(sorted[1]);
            }else{
                if(!outputs[this.upperTo]) outputs[this.upperTo]=[];
                outputs[this.upperTo].push(sorted[1]);
            }
            this.chips = [];
            anyChange = true;
        }
    }
    accept(chip:number){
        this.chips.push(chip);
    }
}
fs.readFile("input/input10.txt", "utf8", (e, data) => {
    
    let lines = data.split("\n");

    lines.forEach(line =>{
        let parts = line.trim().split(' ');
        if(parts[2] === "gives"){
            let newBot : Bot = new Bot();
            newBot.myNumber = parseInt(parts[1]);
            newBot.lowerTo = parseInt(parts[6]);
            newBot.lowerType = parts[5];
            newBot.upperTo = parseInt(parts[11]);
            newBot.upperType = parts[10];
            bots[newBot.myNumber] = newBot;
        }
    });
    lines.forEach(line =>{
        let parts = line.trim().split(' ');
        if(parts[0] === "value"){
            bots[parseInt(parts[5])].accept(parseInt(parts[1]));
        }
    });
    anyChange = true;
    while(anyChange){
        anyChange = false;
        bots.forEach(b => b.do());
    }
    console.log(outputs[0][0] * outputs[1][0] * outputs[2][0]);

});
