var input = "vkjiggvb";
var md5 = require("blueimp-md5");

var start = {x:0,y:0};
var toExpand = [{p:"", c:start}];
var directions = "UDLR";


var diff = {x:[-1,1,0,0], y:[0,0,-1,1]}
function newPos(pos, i){
    return {x:pos.x + diff.x[i], y: pos.y + diff.y[i]};
}

function *nextSteps(pos){
    var hash = md5(input + pos.p);
    for(var i = 0; i<4; i++){
        if(hash[i] > 'a'){
            var next = newPos(pos.c, i);
            if(next.x<0||next.x>3||next.y<0||next.y>3)continue;
            yield {p: pos.p + directions[i], c: next};
        }
    }
}
while(toExpand.length>0){
    var expand = toExpand.shift();
    for(var next of nextSteps(expand)){
        if(next.c.x === 3 && next.c.y === 3){
            console.log("found", next.p.length);
            // toExpand = []
            // break;
        }else{
            toExpand.push(next);

        }
    }
}
console.log("done");