var input = ".^.^..^......^^^^^...^^^...^...^....^^.^...^.^^^^....^...^^.^^^...^^^^.^^.^.^^..^.^^^..^^^^^^.^^^..^";
var lines = 40;

var counter = 0;
function count(arr){counter += arr.reduce((a,v)=>a + (v === '.' ? 1 : 0), 0);}
var currLine = input.split('');
count(currLine);
var prevLine = null;

for(var i = 1; i<lines; i++){
    currLine = currLine.map((a, ix)=>{
        var left = ix === 0 ? '.' : currLine[ix-1];
        var right = ix === currLine.length-1 ? '.' : currLine[ix+1];
        return left === right ? '.' : '^';
    });
    count(currLine);
    console.log(i, counter, currLine.join(''));
}
