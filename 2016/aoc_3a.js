fs = require('fs')

Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};

fs.readFile('input/input3.txt', 'utf8', function (err,data) {
	var lines = data.split("\n");
	var triangles = lines
		.map(function(line){
			var sides = line.split(/\s+/)
				.filter(function(s){return s.length > 0;})
				.map(function(w){return parseInt(w);});
			return sides;
		})
		.filter(function(l){return l.length === 3;})
		.flatMap(function(t, i, arr){
			if(i%3===0){
				return [[arr[i][0],arr[i+1][0],arr[i+2][0]], [arr[i][1],arr[i+1][1],arr[i+2][1]], [arr[i][2],arr[i+1][2],arr[i+2][2]]];
			}else{
				return [];
			}
		})
		.map(function(tri){
			return tri.sort(function(n1,n2){return n1>n2?-1:1});
		});


	var possible = triangles.filter(function(t){return t[0] < t[1] + t[2];})
	console.log(triangles);

	console.log(triangles.length, "triangles");
	console.log(possible.length, "possible");
	console.log(triangles.length-possible.length, "impossible");

});

