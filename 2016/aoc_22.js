var fs = require("fs");
var combinatorics = require('js-combinatorics');
//var fb = require("thenby");

fs.readFile("input/input22.txt", "utf8", (e, data) => {
    var lines = data.trim().split('\n').map(s => s.trim());
    var reParts = /\/dev\/grid\/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s/;
    var nodes = lines.map(l =>{
        var match = reParts.exec(l);
        if(match){
            return {
                x: parseInt(match[1]),
                y: parseInt(match[2]),
                size: parseInt(match[3]),
                used: parseInt(match[4])
            }
        }
        return null;
    }).filter(n => n !== null);
    var pairs = combinatorics.baseN(nodes, 2);
    var viablePairs = pairs
        .filter(p => p[0].used > 0 && p[0].used <= p[1].size - p[1].used && p[0] !== p[1]);
    console.log(viablePairs.length);
});
