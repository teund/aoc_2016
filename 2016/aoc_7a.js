"use strict";
var fs = require("fs");
Array.prototype.flatMap = function(lambda) { 
    return Array.prototype.concat.apply([], this.map(lambda)); 
};
fs.readFile("input/input7.txt", "utf8", function (e, data) {
    // let addresses = [
    //     "ceizefidmvymvyzy[hfhhsjrogfpnpmo]rttainjzgmdphfhfh",
    //     "nzgbxxiimmzsvhe[ixypsgextxvdckbjelq]jpklrvkhgzprfrsv",
    //     "aba[bab]xyz",
    //     "bwzsacxgqkbjycgfw[dbnligvrmqscasutn]rbgybqqsgjvlonkut",
    //     "xyx[xyx]xyx",
    //     "aaa[kek]eke",
    //     "zazbz[bzb]cdb",
    //     "zazzz[zzz]cdb",
    //     "zazbz[yybzbq]cdb",
    //     "zazbz[yybzbq]c[d]b"
    // ];
    addresses = data.split("\n");
    var reBracketed = /\[\w+\]/g;
    var reAba = /(\w)\w\1/g;
    var splitAddresses = addresses
        .filter(function(a){return a.match(reBracketed)})
        .map(function(addr, i){
            console.log("addr", addr);
            return {
                full: addr,
                ix: i,
                inBrackets: addr.match(reBracketed).join(""),
                outsideBrackets: addr.replace(reBracketed, "|")
            }
    });

    var ssl = splitAddresses.filter(function (s, i) {
        var abasOutside = [];
        var m;
        while (m = reAba.exec(s.outsideBrackets)) {
            reAba.lastIndex -= m[0].length - 1;
            abasOutside.push(m[0]);
        }
        var correctAndMatched = abasOutside.filter(function(aba){
            if(aba[0] === aba[1])return false;
            var ok = s.inBrackets.indexOf(aba[1] + aba[0] + aba[1]) > -1;
            if(ok){s.aba = aba;}
            return ok;
        });
        return correctAndMatched.length > 0;

    });
    console.log(ssl);
    console.log(ssl.length, "SSL addresses");
    console.log(ssl.map(function(s){return s.ix}).join(", "));
});
