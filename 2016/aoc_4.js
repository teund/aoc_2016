fs = require('fs')
fb = require('thenby')

String.prototype.countChars = function(s) { 
    return this.split('').reduce(function(acc, c){acc[c] = (acc[c] || 0)+1; return acc;}, {});
};
String.prototype.checksum = function(s) { 
    var counts = this.countChars();
	var chars = Object.keys(counts).sort(fb(function(c){return counts[c];}, -1).thenBy(String));
	return chars.filter(function(c){return c !== "-"}).reduce(function(a,c){return a+c;}, "").substring(0,5);
};

fs.readFile('input/input4.txt', 'utf8', function (err,data) {
	var lines = data.split("\n");
	var codes = lines
		.map(function(line){
			var parts = line.match(/^(\S+)-(\d+)\[(\w{5})\]$/);
			return parts;
		});
	var valid = codes
		.filter(function(parts){return parts !== null && parts.length && parts.length === 4;})
		.filter(function(parts){
			return parts[1].checksum() === parts[3]
		});
	console.log(codes.length, "codes");
	console.log(valid.length, "valid codes");
	console.log("sum", valid.reduce(function(acc, parts){return acc + Number(parts[2])}, 0));
});

