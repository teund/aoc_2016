var input = 3018458;
function FlexArray(){
    var self = this;
    var BLOCK_SIZE = 1000;
    var data = [[]];
    var index = {block:0, item:null, fullIndex:null};
    self.next = function(){
        index.item++;
        if(index.item >= data[index.block].length){
            index.block = (index.block + 1)%data.length;
            index.item = 0;
        }
        index.fullIndex = (index.fullIndex + 1) % _length;
    }
    var _length = 0;
    self.length = function(){return _length;}
    self.push = function(e){
        var lastBlock = data[data.length-1];
        if(lastBlock.length > BLOCK_SIZE){
            data.push([]);
            lastBlock = data[data.length-1];
        }
        _length++;
        lastBlock.push(e);
        if(_length === 1){
            index.item = 0;
            index.fullIndex = 0;
        }
    }
    self.removeNext = function(){
        if(index.item < data[index.block].length-1){
            data[index.block].splice(index.item + 1, 1);
        }else{
            var nextBlock = (index.block + 1) % data.length;
            data[nextBlock].splice(0, 1);
            if(data[nextBlock].length === 0){
                // remove block
                data.splice(nextBlock, 1);
                if(nextBlock === 0){
                    // blockindex must be adjusted
                    index.block--;
                }
            }
        }
        _length--;
    }
    self.removeAcross = function(){
        var opposingIndex = Math.floor((index.fullIndex + _length/2)) % _length;
        var counter = opposingIndex;
        var block = 0;
        while(data[block].length <= counter){
            counter -= data[block].length;
            block++;
        }

        data[block].splice(counter, 1);
        if(opposingIndex < index.fullIndex){
            index.fullIndex--;
        }
        if(data[block].length === 0){
            // remove block
            data.splice(block, 1);
            if(block < index.block){
                // blockindex must be adjusted
                index.block--;
            }
        }
        _length--;
        if(_length % 100000 === 0)console.log("left:", _length);
    }
    
    self.get = function(i){
        var counter = i;
        var block = 0;
        while(data[block].length <= counter){
            counter -= data[block].length;
            block++;
        }
        return data[block][counter];
    }

}

var state = new FlexArray();
for(var i = 1; i<= input; i++){
    state.push(i);
}

while(state.length() > 1){
    // remove next from index
    state.removeAcross();
    state.next();
}
console.log(state.get(0));
