fs = require('fs')
fb = require('thenby')

String.prototype.countChars = function(s) { 
    return this.split('').reduce(function(acc, c){acc[c] = (acc[c] || 0)+1; return acc;}, {});
};
String.prototype.checksum = function(s) { 
    var counts = this.countChars();
	var chars = Object.keys(counts).sort(fb(function(c){return counts[c];}, -1).thenBy(String));
	return chars.filter(function(c){return c !== "-"}).reduce(function(a,c){return a+c;}, "").substring(0,5);
};
String.prototype.rotX = function(x){
	var chars = this.split('');
	return chars.map(function(c){
		if(c === "-")return " ";
		var num = c.charCodeAt(0) - 97;
		return String.fromCharCode((num + x)%26 + 97);
	}).join('');
}


fs.readFile('input/input4.txt', 'utf8', function (err,data) {
	var lines = data.split("\n");
	var codes = lines
		.map(function(line){
			var parts = line.match(/^(\S+)-(\d+)\[(\w{5})\]$/);
			return parts;
		});
	var valid = codes
		.filter(function(parts){return parts !== null && parts.length && parts.length === 4;})
		.filter(function(parts){
			return parts[1].checksum() === parts[3]
		});
	var decrypted = valid.map(function(p){return p[1].rotX(Number(p[2])) + " - " + p[2];});
	console.log(decrypted.sort());
});

