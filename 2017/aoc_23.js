var fs = require('fs');

const _get = (r, v) => {
	if( !isNaN(parseInt(v, 10))) return parseInt(v, 10);
	return r[v] || 0;
}
const ops = {
	'set': (regs, [x,y]) => regs[x] = _get(regs, y),
	'sub': (regs, [x,y]) => regs[x] = _get(regs, x) - _get(regs, y),
	'mul': (regs, [x,y]) => {
		regs[x] = _get(regs, x) * _get(regs, y)
		regs.mul = (regs.mul || 0) + 1;
	},
	'jnz': (regs, [x, y]) => {
		if(_get(regs, x) !== 0) regs.pointer += _get(regs, y) - 1;
	},
};

class Op{
	constructor(str){
		let parts = str.split(' ');
		this.op = ops[parts[0]];
		this.arg = parts.slice(1);
	}
	Do(regs){
		this.op(regs, this.arg);
		regs.pointer++;
	}
}

fs.readFile('2017/input/input23.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	const ops = lines.map(l => new Op(l));
	var regs = {pointer:0, a:1};
	var count = 0;
	while(regs.pointer >= 0 && regs.pointer < ops.length){
		ops[regs.pointer].Do(regs);
		count++;
		if(count % 10000 === 0)console.log(`pointer: ${regs.pointer}, a: ${regs.a}, b: ${regs.b}, c: ${regs.c}, d: ${regs.d}, e: ${regs.e}, f: ${regs.f}, g: ${regs.g}, h: ${regs.h}, `);
	}
	console.log(regs.mul);
});
