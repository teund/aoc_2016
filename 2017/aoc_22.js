var fs = require('fs');

const start = {dir:2, posX: 0, posY:0};
const dirToDelta = [{dx:0, dy:1}, {dx:-1, dy:0}, {dx:0, dy:-1}, {dx:1, dy:0}];
const turn = (now, delta) => (now+delta + 4)%4;
const step = (pos, dir) => {
	return {dir:dir, posX:pos.posX + dirToDelta[dir].dx, posY: pos.posY + dirToDelta[dir].dy};
}
const token = (x,y) => x + "_" + y;
var countInfections = 0;
const dance = (grid, pos)=>{
	var newDir = pos.dir;
	if(token(pos.posX, pos.posY) in grid){
		newDir = turn(pos.dir, +1);
		delete grid[token(pos.posX, pos.posY)];
	}else{
		newDir = turn(pos.dir, -1);
		grid[token(pos.posX, pos.posY)] = true;
		countInfections++;
	}
	return step(pos, newDir);
};

fs.readFile('2017/input/input22.txt', 'utf8', function (err,data) {
	//data = "..#\n#..\n...";
	const chars = data.split("\n").filter(l => l.length > 0)
		.map(l => l.split(''));
	const offset = (chars.length-1)/2;
	var grid = {};
	chars.forEach((arr, y) => arr.forEach((c, x)=> {
		if(c === '#')grid[token(x-offset,y-offset)] = true;
	}));
	var pos = start;
	for (let index = 0; index < 10000; index++) {
		pos = dance(grid, pos);
	}
	console.log(grid);
	console.log(pos);
	console.log(countInfections);

});
