	var fs = require('fs');

	function* groupMapper(it) {
		var depth = 0;
		for (const char of it) {
			if(char==='{'){
				depth++;
				yield depth;
			}
			if(char==='}'){
				depth--;
			}
		}
	}
	function* filterCancelled(it) {
		var result = {done:false};
		while(!result.done) {
			result = it.next();
			if(result.value === '!'){
				it.next();
				continue;
			}
			yield result.value;
		}
	}
	var totalGarbage = 0;	
	function skipOverGarbage(it){
		var streamWithoutCancelled = filterCancelled(it);
		for (const char of streamWithoutCancelled) {
			if(char === '>')break;
			totalGarbage++;
		}
	}
	function* garbageStripper(it) {
		var result = {done:false};

		while(!result.done) {
			result = it.next();
			if(result.value === '<'){
				skipOverGarbage(it);
				continue;
			}
			yield result.value;
		}
	}

	fs.readFile('2017/input/input9.txt', 'utf8', function (err,data) {
		var input = data;
		//input = "{{<!>},{<!>},{<!>},{<a>}}";
		let score = [...groupMapper(
				garbageStripper(input[Symbol.iterator]())
			)]
			.reduce((a,v)=>a+v, 0);
		console.log(score);
		console.log(totalGarbage);

	});
