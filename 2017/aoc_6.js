let fs = require('fs')
const redist = a => {
	var result = a.slice(0);
	const maxIndex = a.indexOf(Math.max(...a));
	const max = a[maxIndex];
	result[maxIndex] = 0;
	const toAll = max / a.length >>0;
	const remain = max % a.length;
	result = result.map(i => i + toAll);
	result = result.map((i, ix) => i + ((a.length + ix - maxIndex -1) % a.length < remain ? 1 : 0));
	return result;
}
const tokenize = a => a.join('-');
const findDup = (a, seen, turns) => {
	if(tokenize(a) in seen){
		return turns;
	}
	seen[tokenize(a)] = true;
	let redistributed = redist(a);
	return findDup(redistributed, seen, turns+1);
} 
const findDup2 = (a) => {
	var seen = {};
	var turns = 0;
	while(true){
		if(tokenize(a) in seen){
			return turns - seen[tokenize(a)];
		}
		seen[tokenize(a)] = turns;
		a = redist(a);
		turns++;
	}
} 
fs.readFile('2017/input/input6.txt', 'utf8', function (err,data) {
	let list = data.split("\t").map(Number);
	//list = [0,2,7,0];
	console.log(findDup(list, {}, 0));

	
});
