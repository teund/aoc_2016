var fs = require('fs');

fs.readFile('2017/input/input2.txt', 'utf8', function (err,data) {
	var lines = data.split("\n").filter(l => l.length > 0);
	lines = lines.map(l => l.split('\t').map(Number).sort((x,y)=>x-y));

    const quot = r => (([x, y]) => y / x)(r.filter(x => r.some(evenDiv(x)))); 
    const evenDiv = x => y => x !== y && (y % x === 0 || x % y === 0); 
    console.log(lines.reduce((sum, r) => sum + quot(r), 0));

});