var fs = require('fs');
const firstBy = require('thenby');
const regexNotNumbers = /[<>=a-z, ]+/;
fs.readFile('2017/input/input20.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0)
		.map(l => l.split(regexNotNumbers).filter(p => p.length).map(Number));
	const particles = lines.map((arr, i) => {
		arr.index = i;
		return arr;
	});
	var best = particles
		.sort(
			firstBy(arr => arr[6]*arr[6] + arr[7]*arr[7] + arr[8]*arr[8])
			.thenBy(arr => arr[3]*arr[3] + arr[4]*arr[4] + arr[5]*arr[5])
			.thenBy(arr => arr[0]*arr[0] + arr[1]*arr[1] + arr[1]*arr[1])
		);
	console.log(best[0]);
});
