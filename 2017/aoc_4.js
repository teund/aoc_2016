fs = require('fs')
fs.readFile('2017/input/input4.txt', 'utf8', function (err,data) {
	var lines = data.split("\n").filter(l => l.length > 0);
	lines = lines.map(l => l.split(' ')
			.map(w => {
				var chars = w.split('');
				return chars.sort().join('');
			})
		);

	const valid = wds => new Set(wds).size === wds.length;

	console.log(lines.filter(valid).length);

});
