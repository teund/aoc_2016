const func = (writeOn0, moveOn0, nextOn0, writeOn1, moveOn1, nextOn1) => (state) => {
	let curr = state.tape[state.pointer] || 0;
	if(curr === 0){
		state.tape[state.pointer] = writeOn0;
		state.pointer += moveOn0;
		state.nextFunc = funcs[nextOn0];
	}else{
		state.tape[state.pointer] = writeOn1;
		state.pointer += moveOn1;
		state.nextFunc = funcs[nextOn1];
	}
}
const funcs = {
	a:func(1, 1, 'b', 0, -1, 'b'),
	b:func(0, 1, 'c', 1, -1, 'b'),
	c:func(1, 1, 'd', 0 ,-1, 'a'),
	d:func(1, -1, 'e', 1, -1, 'f'),
	e:func(1, -1, 'a', 0, -1, 'd'),
	f:func(1, 1, 'a', 1, -1, 'e')	
}

var state = {
	tape:{},
	pointer:0,
	nextFunc:funcs.a
}
for (let turn = 0; turn < 12586542; turn++) {
	state.nextFunc(state);
}
console.log(Object.keys(state.tape).filter(v => state.tape[v] === 1).length);