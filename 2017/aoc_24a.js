const fs = require('fs');
const imm = require('immutable');
class Connector{
	constructor(l, ix){
		this.id = ix;
		this.values = l.split('/').map(Number);
	}
	canConnect(c){
		return this.values.filter(v => v === c).length > 0;
	}
}
class Bridge{
	constructor(smaller, connector){
		this.smaller = smaller;
		this.last = connector;
	}
	get endValue() {
		if(!this._endValue){
			if(!this.smaller){
				this._endValue = 0;
			}else{
				this._endValue = this.last.values[0] === this.smaller.endValue ? this.last.values[1] : this.last.values[0];
			}
		}
		return this._endValue;
	}
	get strength(){
		if(!this._strength){
			if(!this.last){
				this._strength = 0;
			}else{
				this._strength = this.smaller.strength + this.last.values[0] + this.last.values[1];
			}
		}
		return this._strength;
	}
	get length(){
		if(!this._length){
			if(!this.last){
				this._length = 0;
			}else{
				this._length = this.smaller.length + 1;
			}
		}
		return this._length;
	}
	findLongerCombinations(connSet, longest){
		if(this.length > longest.length)longest = this;
		if(this.length === longest.length && this.strength === longest.strength)longest = this;
		for (const conn of connSet) {
			if(conn.canConnect(this.endValue)){
				var longer = new Bridge(this, conn);
				longest = longer.findLongerCombinations(connSet.delete(conn), longest);
			}
		}
		return longest;
	}
}
fs.readFile('2017/input/input24.txt', 'utf8', function (err,data) {
	//data = "0/2\n2/2\n2/3\n3/4\n3/5\n0/1\n10/1\n9/10";
	const connectors = data.split("\n").filter(l => l.length > 0).map((l, ix) => new Connector(l, ix));
	const start = new Bridge();
	const availableConnectors = imm.Set(connectors);
	const longestBridge = start.findLongerCombinations(availableConnectors, start);
	console.log(longestBridge.strength);
});
