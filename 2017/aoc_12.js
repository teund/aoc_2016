var fs = require('fs');
class Node {
	constructor(line){
		var [me, brothers] = line.split("<->");
		this.me = Number(me);
		this.brothers = brothers.split(', ').map(Number);
		this.clan = null;
	}
}
function collectClan(n, nodes){
	var toInspect = [];
	var set = new Set();
	const add = n => {
		if(!set.has(n)){
			set.add(n);
			nodes[n].clan = n;
			nodes[n].brothers.forEach(m => {
				if(!set.has(m))	toInspect.push(m);
			});
		}
	}
	toInspect.push(n);
	while(toInspect.length > 0){
		let inspected = toInspect.pop();
		add(inspected);
	}
	return set;
}

fs.readFile('2017/input/input12.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	const nodes = lines.map(l => new Node(l));

	const clans = nodes.reduce((a,node) => {
		if(node.clan === null){
			a.push(collectClan(node.me, nodes));
		}
		return a;
	}, []
	);
	console.log(clans[0].size);
	console.log(clans.length);
	
});
