var fs = require('fs');
fs.readFile('2017/input/input19.txt', 'utf8', function (err,data) {
	const chars = data.split("\n").filter(l => l.length > 0)
		.map(l => l.split(''));
	const start = {dir:0, posX: chars[0].indexOf('|'), posY:0};
	const dirToDelta = [{dx:0, dy:1}, {dx:-1, dy:0}, {dx:0, dy:-1}, {dx:1, dy:0}];

	const availableNextPositions = (curr) => [0, -1, 1] // (south = 0, west=1, north=2, east=3 )
			.map(d => ((curr.dir + d) + 4) % 4)
			.map(newDir => { 
				return {
					dir:newDir, 
					posX: curr.posX + dirToDelta[newDir].dx, 
					posY: curr.posY + dirToDelta[newDir].dy 
				};
			})
			.filter(pos => chars[pos.posY][pos.posX] !== ' ');
	function* path(from){
		var curr = from;
		while(curr){
			yield curr;
			let nextPos = availableNextPositions(curr);
			curr = nextPos.length ? nextPos[0] : null;
		}
	}
	const fullPath = [...path(start)];
	const word = fullPath.reduce((a,p) => {
		const thisChar = chars[p.posY][p.posX];
		if(thisChar.toLowerCase() != thisChar.toUpperCase()){
			return a + thisChar;
		}
		return a;
	}, "");

	console.log(word);
	console.log(fullPath.length);
});
