var fs = require('fs');

fs.readFile('2017/input/input16.txt', 'utf8', function (err,data) {
	//data = "s1,x3/4,pe/b";
	const tokens = data.split(",").map(s=> s.trim());
	const getMutator = token => {
		const op = token[0];
		const args = token.substring(1);
		switch (op) {
			case 's':
				return t => {
					const snip = t.length - Number(args);
					return t.substring(snip) + t.substring(0, snip);
				}
				case 'x':
				return t => {
					const [pos1, pos2] = args.split('/').map(Number);
					return (t.split('').map((c,i) => i===pos1 ? t[pos2] : i===pos2 ? t[pos1] : t[i])).join('');
				}
				case 'p':
				return t => {
					const [c1, c2] = args.split('/');
					return (t.split('').map((c) => c===c1 ? c2 : c===c2 ? c1 : c)).join('');
				}
			default:
				break;
		}
		return [op, args];
	};
	const mutators = tokens.map(getMutator);
	const totalDances = 1000000000;
	var position = "abcdefghijklmnop";
	var positionsSeen = {};
	positionsSeen[position] = 0;
	for (let index = 0; index < totalDances; index++) {
		position = mutators.reduce((a,f) => f(a), position);
		if(positionsSeen[position]){
			// found loop 
			const loopLength = index - positionsSeen[position] + 1;
			const nrLoops = Math.floor((totalDances - index) / loopLength)
			index += nrLoops*loopLength;
		}
		positionsSeen[position] = index+1;
	}
	console.log(position);
});
