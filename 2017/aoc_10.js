const SIZE = 256;
const input = [97,167,54,178,2,11,209,174,119,248,254,0,255,1,64,190];

// const SIZE = 5;
// const input = [3, 4, 1, 5];
const range = [...Array(SIZE).keys()];

const tranformPos = (p,start) => (p - start + SIZE) % SIZE;
const getMutFn = (curr, l) => (old) => {
	let posFromStart = tranformPos(old, curr);
	if(posFromStart >= l)return old;
	let newPosFromStart = l-posFromStart-1;
	let res = tranformPos(newPosFromStart, -curr);
	return res;
}
var transforms = [];
var skip = 0;
var curr = 0;
for (const l of input) {
	transforms.push(getMutFn(curr, l));
	curr = (curr + l + skip) % SIZE;
	skip++;
}
const fullTransform = (p) => transforms.reduce((a,f)=>f(a), p);
const prod = range.map(i => { return {val:i, pos:fullTransform(i)};}).filter(pair => pair.pos < 2)
	.reduce((a,v) => a*v.val, 1);
console.log(prod);
