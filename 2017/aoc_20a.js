var fs = require('fs');
const firstBy = require('thenby');
const regexNotNumbers = /[<>=a-z, ]+/;

const updateAllParticles = pp => {
	pp.filter(p => p.active).forEach(p => {
		p[3] += p[6];
		p[4] += p[7];
		p[5] += p[8];
		p[0] += p[3];
		p[1] += p[4];
		p[2] += p[5];
	});
}
const terminateCollisions = pp => {
	var found = {};
	pp.filter(p => p.active).forEach(p => {
		var token = p[0] + "_" + p[1] + "_" + p[2];
		if(token in found){
			p.active = false;
			found[token].active = false;
		}
		found[token] = p;
	});
}

fs.readFile('2017/input/input20.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0)
		.map(l => l.split(regexNotNumbers).filter(p => p.length).map(Number));
	const particles = lines.map((arr, i) => {
		arr.index = i;
		arr.active = true;
		return arr;
	});
	var i = 0;
	while(true){
		i++;
		updateAllParticles(particles);
		terminateCollisions(particles);

		console.log(`It: ${i} Active: ${particles.filter(p => p.active).length}`);
	}
});
