fs = require('fs')
fs.readFile('2017/input/input2.txt', 'utf8', function (err,data) {
	var lines = data.split("\n").filter(l => l.length > 0);
	lines = lines.map(l => l.split('\t').map(n => parseInt(n)));

	var maxmins = lines
		.map(l => l.reduce((a,v) => 
	{
		a.min = Math.min(a.min, v);
		a.max = Math.max(a.max, v);
		return a;
	}, {min:+Infinity, max:-Infinity}));

	console.log(maxmins);
	console.log(maxmins);

	var checksum = maxmins.reduce((a,v) => a + v.max - v.min, 0);
	console.log(checksum);
});
