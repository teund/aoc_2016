var fs = require('fs');
var Combi = require('js-combinatorics');

fs.readFile('2017/input/input2.txt', 'utf8', function (err,data) {
	var lines = data.split("\n").filter(l => l.length > 0);
	lines = lines.map(l => l.split('\t').map(n => parseInt(n)));

	var combinationsPerLine = lines
		.map(l => Combi.combination(l, 2)
			.map(p => {return {high:Math.max(p[0], p[1]), low:Math.min(p[0], p[1])}})
		);
	var divisibles = combinationsPerLine
		.map(combis => combis.find(pair => pair.high % pair.low === 0));

	var checksum = divisibles.reduce((a,v) => a + v.high/v.low, 0);
	console.log(checksum);
});
