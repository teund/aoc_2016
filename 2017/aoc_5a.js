let fs = require('fs')
fs.readFile('2017/input/input5.txt', 'utf8', function (err,data) {
	let list = data.split("\n").filter(l => l.length > 0).map(Number);
	var index = 0;
	var steps = 0;
	while(true){
		steps++;
		let toMove = list[index];
		list[index] += toMove >= 3 ? -1 : 1;
		index += toMove;
		if(index < 0 || index >= list.length)break;
	}
	console.log(steps);

});
