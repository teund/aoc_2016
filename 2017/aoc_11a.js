var fs = require('fs');
var astar = require('a-star');

const _diff = {
	"sw": [-1, 1], /* dc, dr */
	"s" : [0 , 2],
	"se": [1 , 1],
	"nw": [-1, -1],
	"n" : [0 , -2],
	"ne": [1 , -1]
};
class Cell{
	constructor(row, col){
		this.row = row;
		this.col = col;
	}
	id(){
		return this.row + "_" + this.col;
	}
	to(direction){
		var [dc, dr] = _diff[direction];
		return new Cell(this.row + dr, this.col + dc);
	}
	*neighbours(){
		yield this.to("sw");
		yield this.to("s");
		yield this.to("se");
		yield this.to("nw");
		yield this.to("n");
		yield this.to("ne");
	}
}

// https://www.redblobgames.com/grids/hexagons/
const w = 1;
const h = Math.sqrt(3) * .5;
const d_row = 0.5*h;
const d_col = 0.75 * w;

fs.readFile('2017/input/input11.txt', 'utf8', function (err,data) {
	const steps = data.split(",").map(s => s.trim());
	const start = new Cell(0,0);
	const [wholePath, end] = steps.reduce((a,v) => {
		let next = a[1].to(v);
		let newSet = a[0].add(next.id());
		return [newSet, next];
	}, [new Set(), start]);

	var toExpand = [start];
	var explored = {};
	explored[start.id()] = 0;
	outerWhile:
	while(true){
		var toExplore = toExpand.shift();
		var path = explored[toExplore.id()];
		for (const neighbor of toExplore.neighbours()) {
			var id = neighbor.id();
			if(!(id in explored)){
				explored[id] = path+1;
				toExpand.push(neighbor);
				if(wholePath.has(id)){
					wholePath.delete(id);
					if(wholePath.size === 0){
						console.log(`Found path to farthest cell: ${path+1}`);
						break outerWhile;
					}
					console.log(`Found 1 step of path. Remaining: ${wholePath.size}`);
				}
			}			
		}
	}
});
