var fs = require('fs');
var astar = require('a-star');

const _diff = {
	"sw": [-1, 1], /* dc, dr */
	"s" : [0 , 2],
	"se": [1 , 1],
	"nw": [-1, -1],
	"n" : [0 , -2],
	"ne": [1 , -1]
};
class Cell{
	constructor(row, col){
		this.row = row;
		this.col = col;
	}
	id(){
		return this.row + "_" + this.col;
	}
	to(direction){
		var [dc, dr] = _diff[direction];
		return new Cell(this.row + dr, this.col + dc);
	}
	*neighbours(){
		yield this.to("sw");
		yield this.to("s");
		yield this.to("se");
		yield this.to("nw");
		yield this.to("n");
		yield this.to("ne");
	}
}

fs.readFile('2017/input/input11.txt', 'utf8', function (err,data) {
	const steps = data.split(",").map(s => s.trim());
	const start = new Cell(0,0);
	const end = steps.reduce((a,v) => a.to(v), start);
	//const end = new Cell(0,-2);
	const result = astar({
		start: start,
		hash: c => c.id(),
		isEnd: c => c.id() === end.id(),
		distance: (a,b) => 1,
		heuristic: (a) => 2*Math.abs(a.col - end.col) + Math.abs(a.row - end.row),
		neighbor: c => [...c.neighbours()]		

	});

	console.log(result.cost);

});
