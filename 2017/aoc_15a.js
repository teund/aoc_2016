const seedA = 634;
const seedB = 301;

function* gen(seed, multiplier, onlyWhen){
	while(true){
		seed = seed * multiplier;
		seed = seed % 2147483647;
		if(seed % onlyWhen === 0)yield seed;
	}
}

const genA = gen(seedA, 16807, 4);
const genB = gen(seedB, 48271, 8);

var matches = 0;

for (let index = 0; index < 5000000; index++) {
	let valA = genA.next().value;
	let valB = genB.next().value;
	if(valA << 16 == valB << 16){
		matches++;
	}
}
console.log(matches);