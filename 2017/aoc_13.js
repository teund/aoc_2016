var fs = require('fs');

fs.readFile('2017/input/input13.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	//const lines = "0: 3_1: 2_4: 4_6: 4".split("_");
	const scanners = lines.reduce((a,v)=> {
		var [depth, range] = v.split(": "); 
		a.set(Number(depth), Number(range));
		return a;
	}, new Map());

	const cycleLength = r => 2*(r - 1);
	const hit = (r, ps) => (ps) % cycleLength(r) === 0;
	const severity = [...scanners].map(kv => {
			const [depth, range] = kv;
			return hit(range, depth) ? range*depth : 0;
		})
		.reduce((a,v) => a+v, 0);
	console.log(severity);
});
