var fs = require('fs');

String.prototype.reverse=function(){return this.split("").reverse().join("");}
const flip = inp => {
	const size = Math.sqrt(inp.length);
	var result = [];
	for (let index = 0; index < size; index++) {
		result = result + inp.substring(size*index, size*(index+1)).reverse();
	}
	return result;
}
const rot = inp => {
	const size = Math.sqrt(inp.length);
	var result = [];
	for (let x = 0; x < size; x++) {
		for (let y = 0; y < size; y++) {
			result[size*x + (size-y)] = inp[size*y+ x];
		}
	}
	return result.join('');
}
function* transitions(inp){
	for (let i = 0; i < 4; i++) {
		inp = rot(inp);
		yield inp;
		yield flip(inp);
	}
}
class Cut{
	constructor(s, x, y){
		this.size = s;
		this.x = x;
		this.y = y;
	}
	expand(from, to, rules){
		var currStr = this.getString(from);
		var newStr = rules[currStr];
		this.writeTo(to, newStr);
	}
	writeTo(grid, str){
		const newSize = Math.sqrt(str.length);
		for (let x = 0; x < newSize; x++) {
			for (let y = 0; y < newSize; y++) {
				grid.set(this.x*newSize + x, this.y*newSize + y, str[newSize*y + x]);
			}
		}
	}
	getString(from){
		var result = [];
		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				result.push(from.get(this.size * this.x + x, this.size * this.y + y));
			}
		}
		return result.join('');
	}

}
class Grid{
	constructor(size){
		this.size = size;
		this._data = [];
	}
	* cuts(){
		const cutSize = this.size%2===0 ? 2 : 3;
		for (let x = 0; x < this.size/cutSize; x++) {
			for (let y = 0; y < this.size/cutSize; y++) {
				yield new Cut(cutSize, x, y, this._data);
			}
		}
	}
	get(x,y){return this._data[this.size*y + x] || '.';}
	set(x,y,c){this._data[this.size*y + x] = c;}
	setFull(str){
		this._data = [...str];
	}
	expand(rules){
		const newSize = this.size % 2 === 0 ? this.size * 3 / 2 : this.size * 4 / 3;
		var newGrid = new Grid(newSize);
		for (const cut of this.cuts()) {
			cut.expand(this, newGrid, rules);
		}
		return newGrid;
	}
	get value(){
		return [...this._data].filter(c => c === '#').length;
	}
}
fs.readFile('2017/input/input21.txt', 'utf8', function (err,data) {
	//data = "../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#";
	const rules = data.split("\n").filter(l => l.length > 0)
		.map(l => l.split(" => ").map(part => part.replace(/\//g,'')));
	const expandedRules = rules.reduce((a,r)=>{
		for (const match of transitions(r[0])) {
			a[match] = r[1];
		}
		return a;
	}, {});
	const start = new Grid(3);
	start.setFull(".#...####");
	var curr = start;
	for (let i = 0; i < 18; i++) {
		curr = curr.expand(expandedRules);
	}

	console.log(curr.value);
});
