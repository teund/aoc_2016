var fs = require('fs');

const ops = {
	'inc': (r, inc) => r + inc,
	'dec': (r, inc) => r - inc,
};
const comps = {
	'<' : (a,b) => a<b,
	'<=': (a,b) => a<=b,
	'>' : (a,b) => a>b,
	'>=': (a,b) => a>=b,
	'==': (a,b) => a===b,	
	'!=': (a,b) => a!==b,	
};
const zeroForUndefined = f => (a,b) => f(a||0, b);
class Op{
	constructor(str){
		let parts = str.split(' ');
		this.reg = parts[0];
		this.op = zeroForUndefined(ops[parts[1]]);
		this.arg = Number(parts[2]);
		this.test = {
			reg:parts[4],
			comp:zeroForUndefined(comps[parts[5]]),
			val:Number(parts[6])	
		}
	}
	Do(regs){
		// OK this is where you really should have an if-statement. This guard is hacky.
		(this.test.comp(regs[this.test.reg], this.test.val))
			&& (regs[this.reg] = this.op(regs[this.reg], this.arg))
	}
}
const highest = regs => Object.keys(regs).map(k => regs[k])
							.sort((a,b) => b-a)[0];

fs.readFile('2017/input/input8.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	const ops = lines.map(l => new Op(l));
	var regs = {};
	var maxEver = 0;
	for (const op of ops) {
		op.Do(regs);
		maxEver = Math.max(maxEver, highest(regs));
	}
	console.log(highest(regs));
	console.log(maxEver);

});
