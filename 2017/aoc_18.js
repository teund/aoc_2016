var fs = require('fs');

const _get = (r, v) => {
	if( !isNaN(parseInt(v, 10))) return parseInt(v, 10);
	return r[v] || 0;
}
const ops = {
	'snd': (regs, [freq]) => regs.freq = _get(regs, freq),
	'set': (regs, [x,y]) => regs[x] = _get(regs, y),
	'add': (regs, [x,y]) => regs[x] = _get(regs, x) + _get(regs, y),
	'mul': (regs, [x,y]) => regs[x] = _get(regs, x) * _get(regs, y),
	'mod': (regs, [x,y]) => regs[x] = _get(regs, x) % _get(regs, y),
	'rcv': (regs, [x]) => {
		if(_get(regs, x) !== 0) console.log(regs.freq)
	},
	'jgz': (regs, [x, y]) => {
		if(_get(regs, x) > 0) regs.pointer += _get(regs, y) - 1;
	},
};

class Op{
	constructor(str){
		let parts = str.split(' ');
		this.op = ops[parts[0]];
		this.arg = parts.slice(1);
	}
	Do(regs){
		this.op(regs, this.arg);
		regs.pointer++;
	}
}

fs.readFile('2017/input/input18.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	const ops = lines.map(l => new Op(l));
	var regs = {pointer:0};
	while(regs.pointer >= 0 && regs.pointer < ops.length){
		ops[regs.pointer].Do(regs);
	}
});
