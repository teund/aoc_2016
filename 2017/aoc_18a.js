var fs = require('fs');

const _get = (r, v) => {
	if( !isNaN(parseInt(v, 10))) return parseInt(v, 10);
	return r[v] || 0;
}
const ops = {
	'snd': (regs, [freq]) => {
		regs.outqueue.push(_get(regs, freq));
		regs.sent++;
	},
	'set': (regs, [x,y]) => regs[x] = _get(regs, y),
	'add': (regs, [x,y]) => regs[x] = _get(regs, x) + _get(regs, y),
	'mul': (regs, [x,y]) => regs[x] = _get(regs, x) * _get(regs, y),
	'mod': (regs, [x,y]) => regs[x] = _get(regs, x) % _get(regs, y),
	'rcv': (regs, [x]) => {
		if(regs.inqueue.length){
			regs[x] = regs.inqueue.shift();
			regs.blocked = false;
		}else{
			regs.pointer--;
			regs.blocked = true;
		}
	},
	'jgz': (regs, [x, y]) => {
		if(_get(regs, x) > 0) regs.pointer += _get(regs, y) - 1;
	},
};

class Op{
	constructor(str){
		let parts = str.split(' ');
		this.op = ops[parts[0]];
		this.arg = parts.slice(1);
	}
	Do(regs){
		this.op(regs, this.arg);
		regs.pointer++;
	}
}

fs.readFile('2017/input/input18.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);
	const ops = lines.map(l => new Op(l));
	var regs0 = {pointer:0, outqueue:[], p:0, blocked:false, sent:0};
	var regs1 = {pointer:0, outqueue:[], p:1, blocked:false, sent:0};
	regs0.inqueue = regs1.outqueue;
	regs1.inqueue = regs0.outqueue;
	while(regs0.blocked === false || regs1.blocked === false){
		if(regs0.pointer >= 0 && regs0.pointer < ops.length){
			ops[regs0.pointer].Do(regs0);
		}else{
			regs0.blocked = true;
		}
		if(regs1.pointer >= 0 && regs1.pointer < ops.length){
			ops[regs1.pointer].Do(regs1);
		}else{
			regs1.blocked = true;
		}
	}
	console.log(regs1.sent);
});
