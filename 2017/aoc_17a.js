const input = 363;
//const input = 3;
const cycles = 50000000;
var listLength = 1;
var currentPos = 0;
var valueAfter0 = null;
for (let index = 1; index <= cycles; index++) {
	currentPos = (currentPos + input) % listLength;
	if(currentPos === 0)valueAfter0=index;
	listLength++;
	currentPos++; 
	if(listLength%100000 === 0)console.log(`Done with cycle ${index}`);
}
console.log(valueAfter0);