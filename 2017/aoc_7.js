var fs = require('fs');

const regex = /([a-z]{3,20}) \((\d+)\)(?: -> (.*))?/;
class Program{
	constructor(name, weight, tokens){
		this.name = name;
		this.weight = Number(weight);
		this.tokens = tokens;
		this.parent = null;
		this._all = {};
		this._cachedCumulWeight = null;
	}
	get childTokens(){
		if(!this.tokens)return [];
		return this.tokens.split(', ');
	}
	connectChildren(all){
		this._all = all;
		for (const c of this.childTokens) {
			all[c].setParent(this);
		}
	}
	setParent(p){
		this.parent = p;
	}
	get cumulWeight(){
		if(this._cachedCumulWeight===null)
		{
			this._cachedCumulWeight = this.childTokens
			.map(t => this._all[t])
			.reduce((a,v)=>a+v.cumulWeight, this.weight);
		}
		return this._cachedCumulWeight;
	}
	printTree(unbalancedOnly, prefix){
		console.log(`${prefix}${this.name}: total weight: ${this.cumulWeight}`)
		const uniqueChildWeights = new Set(this.childTokens.map(t => this._all[t].cumulWeight));
		if(unbalancedOnly === false || uniqueChildWeights.size > 1)
		{
			for (const c of this.childTokens) {
				this._all[c].printTree(unbalancedOnly, prefix + '--');
			}
		}
	}
}

fs.readFile('2017/input/input7.txt', 'utf8', function (err,data) {
	const lines = data.split("\n").filter(l => l.length > 0);

	const programsRead = lines.map(l => {
		const res = regex.exec(l);
		return new Program(res[1], res[2], res[3]);
	});
	const allPrograms = programsRead.reduce((a,v)=>{a[v.name] = v; return a}, {});
	programsRead.forEach(p => p.connectChildren(allPrograms));
	var p = programsRead.find(p => p.parent === null);

	p.printTree(true, "");
});
