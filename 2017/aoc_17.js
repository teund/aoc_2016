const input = 363;
//const input = 3;
const cycles = 2017;
var list = [0];
var currentPos = 0;
for (let index = 1; index <= cycles; index++) {
	currentPos = (currentPos + input) % list.length;
	list = list.slice(0, currentPos+1).concat(index).concat(list.slice(currentPos+1));
	currentPos++; 
}
console.log(list[currentPos+1]);