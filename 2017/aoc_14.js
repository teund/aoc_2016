var fs = require('fs');
var kh = require('./knothash')

const input = 'amgozmfv';

const rowNumbers = Array.from({length:128}, (v,k)=> k);
const hashes = rowNumbers.map(n => kh.hash(input + '-' + n));
const bitArrays = hashes.map(hash =>{
	var string_1_0 =  hash.split('').map(c => ('000' + (parseInt(c, 16)).toString(2)).slice(-4)).join('');
	return string_1_0.split('').map(c => c==='1');
});
const sum = bitArrays.reduce((a,v) => a + v.reduce((ia,iv)=>ia + Number(iv), 0), 0);
console.log(sum);

function* neighbours(x,y){
	if(x < 127)yield [x+1, y];
	if(x>0)yield [x-1, y];
	if(y<127)yield [x, y+1];
	if(y>0)yield [x, y-1];
}
const token = (x,y) => x + "_" + y; 
function addToAdjacents(x,y,bitArrays, visited){
	visited.add(token(x,y));
	for (const [nx,ny] of neighbours(x,y)) {
		if(bitArrays[ny][nx] === false)continue;
		if(!visited.has(token(nx,ny))){
			addToAdjacents(nx, ny, bitArrays, visited);
		}
	}
}
var clusters = {};
var nextCluster = 1;
for(var x = 0; x< 128; x++){
	for(var y = 0; y< 128; y++){
		if(bitArrays[y][x] === false)continue;
		const myToken = token(x, y);
		if(clusters[myToken]){
			continue; // already in a token
		}
		const thisCluster = nextCluster++;
		var visited = new Set();
		addToAdjacents(x,y,bitArrays, visited);
		for (const key of visited.keys()){
			clusters[key] = thisCluster;
		}
		
		
	}
}
console.log(nextCluster - 1)