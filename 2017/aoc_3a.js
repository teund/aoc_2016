let input = 347991;

const endOfCycle = c => Math.pow(2*c + 1, 2);
const cycleForIndex = i => Math.ceil((Math.sqrt(i) - 1)/2);

const coordForIndexInCycle = cycle => ic => {
    let seg = Math.floor(ic/(cycle*2)) % 4; // this % 4 is only neccessary for the final cell of a cycle
    let indexInSegment = ic % (cycle*2);
        
    return [
        [cycle, -cycle + indexInSegment],
        [cycle-indexInSegment, cycle],
        [-cycle, cycle - indexInSegment],
        [-cycle + indexInSegment, -cycle]
    ][seg];
    }

const indexToCoord = i => {
    let cycle = cycleForIndex(i);
    let indexInCycle = i - endOfCycle(cycle-1);
    return coordForIndexInCycle(cycle)(indexInCycle);
}
const coordToIndex = ([x,y]) =>{
    let c = Math.max(Math.abs(x), Math.abs(y));
    let startOfCycle = endOfCycle(c-1);
    if(y === -c){
        return startOfCycle + 6*c + (c+x);
    }
    if(x === -c){
        return startOfCycle + 4*c + (c-y);
    }
    if(y === c){
        return startOfCycle + 2*c + (c-x);
    }
    return startOfCycle + (c+y);
}
function* neighbours(coord){
    yield [coord[0] - 1, coord[1] + 1];
    yield [coord[0] - 1, coord[1] + 0];
    yield [coord[0] - 1, coord[1] - 1];
    yield [coord[0]    , coord[1] + 1];
    yield [coord[0]    , coord[1] - 1];
    yield [coord[0] + 1, coord[1] + 1];
    yield [coord[0] + 1, coord[1] + 0];
    yield [coord[0] + 1, coord[1] - 1];
}


// no tail recursion, so...
var results = [NaN, 1];
while(true){
    let index = results.length;
    let coord = indexToCoord(index);

    results[index] = [...neighbours(coord)].reduce((a, v) => {
        let nIndex = coordToIndex(v);
        return a + ((nIndex < index) ? results[nIndex] : 0);
    }, 0);
    console.log(`${index}: ${results[index]}`);
    if(results[index] > input)break;
}