declare module "ndarray"
{
    export function ndarray(data: Data, shape?: number[], stride?: number[], offset?: number): NdArray;

    type Data =
        Array<number> | Int8Array | Int16Array | Int32Array |
        Uint8Array | Uint16Array | Uint32Array |
        Float32Array | Float64Array | Uint8ClampedArray;

    export interface NdArray {
        data: Data;
        shape: number[];
        stride: number[];
        offset: number;
        dtype: 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' |
        'float32' | 'float64' | 'array' | 'uint8_clamped' | 'buffer' | 'generic';
        size: number;
        order: number[];
        dimension: number;
        get(...args: number[]): number;
        set(...args: number[]): number;
        index(...args: number[]): number;
        lo(...args: number[]): NdArray;
        hi(...args: number[]): NdArray;
        step(...args: number[]): NdArray;
        transpose(...args: number[]): NdArray;
        pick(...args: number[]): NdArray;
    }
}
declare module "zeros"
{
    import {NdArray} from "ndarray";
    export function zeros(shape: number[], dt?: 'int8' | 'int16' | 'int32' | 'uint8' | 'uint16' | 'uint32' |
    'float32' | 'float64' | 'array' | 'uint8_clamped' | 'buffer' | 'generic'): NdArray
}

declare module "save-pixels";

interface astarCbs<T>{
    id(node: T): string;
    // It should check: is a node is the goal?
    isGoal(node: T): boolean;
    // It should return an array of successors / neighbors / children
    getSuccessors(node: T): T[];
    // g(x). It should return the cost of path between two nodes
    distance(nodeA: T, nodeB: T): number;
    // h(x). It should return the low estimated cost of path from a node to the goal
    estimate(node: T, goal: T): number
}
declare module "astar-algorithm" {
    function astar<T>(start: T, goal: T, callbacks: astarCbs<T>): T[];
    export = astar;
}